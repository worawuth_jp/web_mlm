import axios from 'axios';
import ErrorModel from '../models/errorModel';

class CoreService {
  _url = `${process.env.BASE_API_PATH}/api`;
  _header = {};

  async callService(path, method, data = {}) {
    try {
      let url = this._url + path;
      this._header = {
        'x-api-version': process.env.API_VERSION,

        'Content-Type': 'application/json',
      };
      //await this.refreshToken();
      let result = await axios({
        url: url,
        headers: this._header,
        data: JSON.stringify(data),
        method: method,
      });
      return result;
    } catch (error) {
      console.error('error call Services');
      throw error;
    }
  }

  async login(username, password) {
    try {
      let result = await this.callService('/login', 'post', { username, password });
      return result.data.result;
    } catch (error) {
      throw new ErrorModel(error.response.data.statusCode, error.response.data.statusMessage);
    }
  }

  async getAllMemberUnderTeam(memberId) {
    try {
      let result = await this.callService(`/member-under-team?memberId=${memberId}`, 'get');
      return result.data.result;
    } catch (error) {
      throw new ErrorModel(error.response.data.statusCode, error.response.data.statusMessage);
    }
  }

  async createMember(data) {
    try {
      let result = await this.callService(`/members`, 'post', data);
      return result.data.result;
    } catch (error) {
      throw new ErrorModel(error.response.data.statusCode, error.response.data.statusMessage);
    }
  }

  async changePassword(data) {
    try {
      let result = await this.callService(`/change-password`, 'post', data);
      return result.data.result;
    } catch (error) {
      throw new ErrorModel(error.response.data.statusCode, error.response.data.statusMessage);
    }
  }
}

export default CoreService;
