const path = require('path');
module.exports = {
  reactStrictMode: true,
  env: {
    BASE_API_PATH: process.env.BASE_API_PATH,
    API_VERSION: process.env.API_VERSION,
  },
};
