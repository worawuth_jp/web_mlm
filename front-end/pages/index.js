import IndexContainer from '../components/containers/IndexContainer';
import MainLayout from '../components/layouts/mainLayout';
import IndexStyle from './index/style';

export default function Home() {
  return (
    <IndexStyle>
      <MainLayout page="index">
        <IndexContainer />
      </MainLayout>
    </IndexStyle>
  );
}
