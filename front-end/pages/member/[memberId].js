import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import MemberContainer from '../../components/containers/memberContainer';
import MainLayout from '../../components/layouts/mainLayout';
import MemberPageStyle from './style';

export default function Member() {
  let router = useRouter();
  const { memberId } = router.query;

  useEffect(() => {
    if (memberId === '$refresh') {
      router.push('/member/' + localStorage.getItem('memberId') ? localStorage.getItem('memberId') : 'null');
    }
  }, [memberId]);
  return (
    <MemberPageStyle>
      {memberId && (
        <MainLayout page="member">
          <MemberContainer memberId={memberId} />
        </MainLayout>
      )}
    </MemberPageStyle>
  );
}
