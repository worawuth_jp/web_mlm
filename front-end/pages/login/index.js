import React from 'react';
import LoginContainer from '../../components/containers/loginContainer/index';
import LoginLayout from '../../components/layouts/loginLayout/index';
import LoginPageStyle from './style';

const LoginPage = () => {
  return (
    <LoginPageStyle>
      <LoginLayout>
        <LoginContainer />
      </LoginLayout>
    </LoginPageStyle>
  );
};

export default LoginPage;
