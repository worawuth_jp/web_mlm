import Head from 'next/head';
import { Fragment } from 'react';
import { wrapper } from '../redux/stores';
import GlobalStyle from '../styles/globals';

function MyApp({ Component, pageProps }) {
  return (
    <Fragment>
      <GlobalStyle />
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, shrink-to-fit=no"></meta>
        {/* Font */}
        {/* <link rel="stylesheet" type="text/css" href="/font.css"></link> */}
      </Head>

      <Component {...pageProps} />
    </Fragment>
  );
}

export default wrapper.withRedux(MyApp);
