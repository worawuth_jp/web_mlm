import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import MainLayout from '../../components/layouts/mainLayout';
import LoadingDialog from '../../components/shared/loading';

export default function Logout() {
  const router = useRouter();
  useEffect(() => {
    sessionStorage.clear();
    localStorage.clear();

    router.replace('/login');
  }, []);
  return (
    <MainLayout>
      <LoadingDialog open={true} />
    </MainLayout>
  );
}
