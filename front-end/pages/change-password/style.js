const { default: styled } = require('styled-components');

const ChangePasswordPageStyle = styled.main`
  display: flex;
`;

export default ChangePasswordPageStyle;
