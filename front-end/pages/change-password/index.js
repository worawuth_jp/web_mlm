import React from 'react';
import ChangePasswordContainer from '../../components/containers/changePasswordContainer';
import MainLayout from '../../components/layouts/mainLayout';
import ChangePasswordPageStyle from './style';

const ChangePassword = (props, ref) => {
  return (
    <ChangePasswordPageStyle>
      <MainLayout page="changePassword">
        <ChangePasswordContainer />
      </MainLayout>
    </ChangePasswordPageStyle>
  );
};

export default ChangePassword;
