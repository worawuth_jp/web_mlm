export const menu = [
  { link: '/', name: 'หน้าแรก', key: 'index' },
  { link: `/member/$refresh`, name: 'สายงาน', key: 'member' },
  { link: `/change-password`, name: 'เปลี่ยนรหัสผ่าน', key: 'changePassword' },
  { link: '/logout', name: 'ออกจากระบบ', key: 'logout' },
];
