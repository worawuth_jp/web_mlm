import classNames from 'classnames';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import NavbarStyle from './style';
import MenuIcon from '@mui/icons-material/Menu';
import * as constants from '../../../constants/navbarConstants';

const Navbar = React.forwardRef((props, ref) => {
  const [open, setOpen] = useState(false);
  return (
    <NavbarStyle ref={ref} open={open}>
      <div className="nav">
        <div className="logo-wrapper">
          <Link href="/">
            <img className="logo" src="/images/logo.png" />
          </Link>
        </div>
        <div className="nav-wrapper">
          {constants.menu.map((menu, index) => (
            <div key={`MENU_${menu.key}`} className={classNames('nav-item', { active: props.active === menu.key })}>
              <Link href={menu.link}>{menu.name}</Link>
            </div>
          ))}
          {/* <div className={classNames('nav-item', { active: true })}>
            <Link href="/">หน้าแรก</Link>
          </div>
          <div className={classNames('nav-item')}>
            <Link href="/member">สายงาน</Link>
          </div>
          <div className={classNames('nav-item')}>
            <Link href="/logout">ออกจากระบบ</Link>
          </div> */}
        </div>
      </div>
      <div className="nav-mobile">
        <div className="nav-icon" onClick={() => setOpen((prev) => !prev)}>
          <MenuIcon />
        </div>
        <div className="logo-wrapper">
          <Link href="/">
            <img className="logo" src="/images/logo.png" />
          </Link>
        </div>
      </div>

      <div className="nav-menu">
        <div className="nav-wrapper">
          {constants.menu.map((menu, index) => (
            <div
              key={`MENU_${menu.key}`}
              onClick={() => {
                if (props.active === menu.key) {
                  setOpen(false);
                }
              }}>
              <Link href={menu.link}>
                <div className={classNames('nav-item', { active: props.active === menu.key })}>{menu.name}</div>
              </Link>
            </div>
          ))}
          {/* <div className={classNames('nav-item', { active: true })}>
            <Link href="/">หน้าแรก</Link>
          </div>
          <div className={classNames('nav-item')}>
            <Link href="/member">สายงาน</Link>
          </div>
          <div className={classNames('nav-item')}>
            <Link href="/logout">ออกจากระบบ</Link>
          </div> */}
        </div>
      </div>
    </NavbarStyle>
  );
});

export default Navbar;
