import styled from 'styled-components';
import border from '../../../styles/variables/border';
import color from '../../../styles/variables/color';
import navbar from '../../../styles/variables/navbar';

const NavbarStyle = styled.nav`
  background: ${color.PRIMARY};
  height: ${navbar.HEIGHT};
  position: absolute;
  top: 0;
  width: 100%;

  .nav {
    display: flex;
    height: ${navbar.HEIGHT};
    padding: 0px 25px;
  }

  .active {
    background: ${color.SECONDARY};
    border-radius: ${border.BORDER_RADIUS_PX};
  }

  .nav-mobile {
    display: none;
    min-width: 200px;
    padding: 0px 25px;
    position: relative;
    height: ${navbar.HEIGHT};
  }

  .nav-menu {
    z-index: 2;
    height: calc(100vh - ${navbar.HEIGHT});
    top: ${navbar.HEIGHT};
    min-width: 100vw;
    position: absolute;
    /* left: 0; */
    left: ${(props) => (props.open ? '0' : '-100vw')};
    background: ${color.PRIMARY};

    & .nav-wrapper {
      display: block;
      padding: 15px 10px;
    }
  }

  .open {
    & .nav-menu {
      left: 0;
    }
  }

  .logo-wrapper {
    display: flex;
    align-items: center;
    cursor: pointer !important;
    height: auto;
    margin: auto;
  }

  .logo {
    height: 50px;
    background: ${color.WHITE_COLOR};
    padding: 0px 25px;
  }

  .nav-wrapper {
    display: flex;
    color: ${color.GRAY_COLOR_1};
    align-items: center;
    justify-content: end;
    width: 100%;
  }

  .nav-icon {
    padding: 5px;
    background: ${color.WHITE_COLOR};
    height: auto;
    position: absolute;
    top: 12px;
  }

  .nav-icon-open {
    padding: 5px;
    background: ${color.WHITE_COLOR};
    height: auto;
    position: absolute;
    top: 12px;
  }

  .nav-item {
    padding: 5px 25px;
    display: flex;
    align-content: flex-end;
  }

  //xs
  @media screen and (max-width: 596px) {
    & .nav {
      display: none;
    }

    & .nav-mobile {
      display: flex;
    }

    & .logo-wrapper {
      display: flex;
      justify-content: center;
      align-content: center;
      width: 100%;
    }
  }

  //sm
  @media screen and (min-width: 600px) and (max-width: 899px) {
    & .nav {
      display: none;
    }

    & .nav-mobile {
      display: flex;
    }
  }

  //md
  @media screen and (min-width: 900px) and (max-width: 1159px) {
    & .nav {
      display: none;
    }

    & .nav-mobile {
      display: flex;
    }
  }

  //lg
  @media screen and (min-width: 1200px) and (max-width: 1535px) {
  }

  //xxl
  @media screen and (min-width: 1920px) {
  }
`;

export default NavbarStyle;
