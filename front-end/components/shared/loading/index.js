import { CircularProgress, Dialog } from '@mui/material';
import React from 'react';
import LoadingStyle from './style';

export default function LoadingDialog({ open = false }) {
  return (
    <Dialog open={open} maxWidth="xs" disableEscapeKeyDown>
      <LoadingStyle>
        <CircularProgress style={{ margin: 'auto' }} size={100} />
      </LoadingStyle>
    </Dialog>
  );
}
