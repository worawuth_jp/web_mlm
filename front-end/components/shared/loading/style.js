const { default: styled } = require('styled-components');

const LoadingStyle = styled.div`
  min-height: 150px;
  min-width: 150px;
  display: flex;
  justify-content: center;
  justify-items: center;

  .loading {
    align-items: center;
  }
`;

export default LoadingStyle;
