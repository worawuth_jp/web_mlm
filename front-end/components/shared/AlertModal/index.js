import { Button, Dialog, DialogActions, DialogContent } from '@mui/material';
import React from 'react';
import AlertModalStyle from './style';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import WarningIcon from '@mui/icons-material/Warning';
import ErrorIcon from '@mui/icons-material/Error';
import PrivacyTipIcon from '@mui/icons-material/PrivacyTip';

const AlertModal = React.forwardRef((props, ref) => {
  return (
    <Dialog maxWidth="xs" open={props.open} onClose={props.onClose} disableEscapeKeyDown={props.confirm}>
      <AlertModalStyle>
        <DialogContent style={{ textAlign: 'center' }}>
          {props.success && <CheckCircleIcon className="icon success" />}
          {props.warning && <WarningIcon className="icon warning" />}
          {props.error && <ErrorIcon className="icon error" />}
          {props.confirm && <PrivacyTipIcon className="icon confirm" />}
          <div className="subject">{props.subject}</div>
          <div className="message">{props.message}</div>
        </DialogContent>
        <DialogActions>
          {(props.error || props.warning || props.success) && (
            <Button variant="contained" fullWidth className="btn-button btn-one" onClick={props.onClose}>
              ปิด
            </Button>
          )}

          {props.confirm && (
            <div className="action">
              <Button variant="contained" color="error" className="btn-button" onClick={props.onClose}>
                ยกเลิก
              </Button>

              <Button variant="contained" color="primary" style={{ marginLeft: 'auto' }} className="btn-button" onClick={props.onSubmit}>
                ตกลง
              </Button>
            </div>
          )}
        </DialogActions>
      </AlertModalStyle>
    </Dialog>
  );
});

export default AlertModal;
