import color from '../../../styles/variables/color';

const { default: styled } = require('styled-components');

const AlertModalStyle = styled.div`
  min-height: 250px;
  min-width: 400px;
  overflow-y: auto;
  .subject {
    font-size: 18px;
    font-weight: 800;
  }

  .btn-one {
    background: ${color.GRAY_COLOR_3};
  }

  .action {
    display: flex;
    width: 100%;
  }

  .message {
    font-size: 16px;
  }

  .icon {
    font-size: 120px;
  }

  .success {
    color: #33935b;
  }

  .warning {
    color: #ffc446;
  }

  .error {
    color: #db5745;
  }

  .confirm {
    color: #9c9797;
  }

  //xs
  @media screen and (max-width: 596px) {
    min-height: 200px;
    min-width: 300px;
  }

  //sm
  @media screen and (min-width: 600px) and (max-width: 899px) {
    min-height: 200px;
    min-width: 400px;
  }

  //md
  @media screen and (min-width: 900px) and (max-width: 1159px) {
    min-height: 200px;
    min-width: 400px;
  }

  //lg
  @media screen and (min-width: 1200px) and (max-width: 1535px) {
  }

  //xxl
  @media screen and (min-width: 1920px) {
  }
`;

export default AlertModalStyle;
