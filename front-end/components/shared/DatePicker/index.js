import * as React from 'react';
import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import dayjs from 'dayjs';

export default function DatePickerInput({ label, value, setValue }) {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        label="เลือกวันที่"
        value={value}
        rifmFormatter={(dateStr) => dayjs(dateStr).format('DD/MM/YYYY')}
        onChange={(newValue) => {
          setValue(newValue);
        }}
        renderInput={(params) => <TextField style={{ marginTop: 10 }} size="medium" {...params} />}
      />
    </LocalizationProvider>
  );
}
