import React from 'react';
import SidebarStyle from './style';

const Sidebar = React.forwardRef((props, ref) => {
  return (
    <SidebarStyle screenHeight={props.screenHeight}>
      <div className="sidebar-wrapper"></div>
    </SidebarStyle>
  );
});

export default Sidebar;
