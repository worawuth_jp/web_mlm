import styled from 'styled-components';

const SidebarStyle = styled.div`
  .sidebar-wrapper {
    height: ${(props) => props.screenHeight * 85}px;
    width: 200px;
    background: #1e1d1d;
  }
`;

export default SidebarStyle;
