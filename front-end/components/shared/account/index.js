import React from 'react';
import AccountStyle from './style';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import color from '../../../styles/variables/color';

const Account = React.forwardRef((props, ref) => {
  return (
    <AccountStyle root={props.root} cardWidth={props.cardWidth}>
      <div className="icon">
        <AccountCircleIcon style={{ fontSize: 85, color: !props.root ? color.RED_COLOR : color.GRAY_COLOR_3 }} />
      </div>
      <div className="header-title">{props.memberId}</div>
      <div className="header-content">
        {props.firstname} {props.lastname}
      </div>
    </AccountStyle>
  );
});

export default Account;
