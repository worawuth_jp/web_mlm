import styled from 'styled-components';
import color from '../../../styles/variables/color';

const AccountStyle = styled.div`
  padding: 5px 25px;
  text-align: center;
  width: calc(${(props) => (props.cardWidth - 20) / 8}px);

  :hover {
    background: ${(props) => (props.root ? 'transparent' : color.GRAY_COLOR_1)};
    cursor: ${(props) => (props.root ? 'default' : 'pointer')};
  }
  .icon {
    margin-top: 10px;
  }

  .header-title {
    display: flex;
    font-size: 16px;
    font-weight: 700;
    justify-content: center;
  }

  .header-content {
    justify-content: center;
  }

  //xs
  @media screen and (max-width: 596px) {
    //width: auto;
    width: calc(${(props) => (props.cardWidth - 20) / 2}px);
  }

  //sm
  @media screen and (min-width: 600px) and (max-width: 899px) {
    width: calc(${(props) => (props.cardWidth - 20) / 4}px);
  }

  //md
  @media screen and (min-width: 900px) and (max-width: 1159px) {
    width: calc(${(props) => (props.cardWidth - 20) / 5}px);
  }

  //lg
  @media screen and (min-width: 1200px) and (max-width: 1535px) {
    width: calc(${(props) => (props.cardWidth - 20) / 6}px);
  }

  //xxl
  @media screen and (min-width: 1920px) {
    width: calc(${(props) => (props.cardWidth - 20) / 10}px);
  }
`;

export default AccountStyle;
