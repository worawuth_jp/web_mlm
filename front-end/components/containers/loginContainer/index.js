import { Button, TextField } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeAlert } from '../../../redux/globalRedux/action';
import { fetchLogin } from '../../../redux/memberRedux/action';
import AlertModal from '../../shared/AlertModal';
import LoadingDialog from '../../shared/loading';
import LoginContainerStyle from './style';

const LoginContainer = React.forwardRef((props, ref) => {
  const initial = {
    username: '',
    password: '',
  };
  const dispatch = useDispatch();
  const router = useRouter();
  const [memberLogin, setMemberLogin] = useState(initial);
  const [error, setError] = useState({});
  const isLoading = useSelector((state) => state.global.options.isLoading);
  const alert = useSelector((state) => state.global.options.alert);
  const member = useSelector((state) => state.member.memberLogin);

  const validate = () => {
    let err = {};
    if (memberLogin.username === '') {
      err.username = 'กรุณาใส่เลขบัตรประชาชน/เบอร์โทรศัพท์';
      err.isError = true;
    }

    if (memberLogin.password === '') {
      err.password = 'กรุณาใส่รหัสผ่าน';
      err.isError = true;
    }

    setError({ isSubmit: true, ...err });

    return { isSubmit: true, ...err };
  };

  const handleChange = (event) => {
    let name = event.target.name;
    let temp = { ...memberLogin };
    temp[name] = event.target.value;
    setMemberLogin({ ...temp });
  };

  const handleSubmit = (e) => {
    if (!validate().isError) {
      memberLogin.isSubmit = true;
      dispatch(fetchLogin(memberLogin.username, memberLogin.password));
    }

    e.preventDefault();
  };

  const closeAlertModal = () => {
    dispatch(closeAlert());
    if (alert.success) {
      router.replace('/');
    }
  };

  useEffect(() => {
    if (member.isLogin && memberLogin.isSubmit) {
      router.replace('/');
    }
  }, [member.isLogin, memberLogin.isSubmit]);

  return (
    <LoginContainerStyle>
      <LoadingDialog open={isLoading} />
      <AlertModal open={alert.open} onClose={() => closeAlertModal()} success={alert.success} error={!alert.success} subject={alert.subject} message={alert.message} />
      <div className="card">
        <div className="card-header">
          <img src="/images/logo.png" className="logo" />
          <div>Sign In</div>
        </div>
        <div className="card-detail">
          <form onSubmit={handleSubmit}>
            <div className="error-msg">{error.isSubmit && error.isError && error.username}</div>
            <TextField
              name="username"
              onChange={handleChange}
              value={memberLogin.username}
              label="รหัสบัตรประชาชน/เบอร์โทรศัพท์"
              className="input-space"
              variant="outlined"
              fullWidth
            />

            <div className="error-msg">{error.isSubmit && error.isError && error.password}</div>
            <TextField name="password" onChange={handleChange} value={memberLogin.password} label="รหัสผ่าน" variant="outlined" className="input-space" type="password" fullWidth />
            <Button type="submit" className="btn-button" variant="contained">
              เข้าสู่ระบบ
            </Button>
          </form>
        </div>
      </div>
    </LoginContainerStyle>
  );
});

export default LoginContainer;
