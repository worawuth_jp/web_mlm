import styled from 'styled-components';
import border from '../../../styles/variables/border';
import color from '../../../styles/variables/color';
import font from '../../../styles/variables/font';

const LoginContainerStyle = styled.div`
  .card {
    background: ${color.WHITE_COLOR};
    min-height: 400px;
    min-width: 400px;
    max-width: 450px;
    margin-top: -25%;
    border-radius: ${border.BORDER_RADIUS_PX};
  }

  .card-header {
    text-align: center;

    div {
      font-size: ${font.FONT_HEADER_SIZE_PX};
      font-weight: bold;
    }
  }

  & .btn-button {
    margin: auto;
  }

  .card-detail {
    text-align: center;
    justify-content: center;
    padding: 5%;
  }

  .logo {
    width: 100px;
  }

  .error-msg {
    color: red;
    font-size: 14px;
    display: block;
    text-align: left;
  }

  //xs
  @media screen and (max-width: 596px) {
    & .card {
      min-height: 200px;
      min-width: 200px;
      max-width: 300px;
    }

    & .logo {
      width: 80px;
    }
  }

  //sm
  @media screen and (min-width: 600px) and (max-width: 899px) {
    & .card {
      min-height: 200px;
      min-width: 300px;
      max-width: 350px;
    }

    & .logo {
      width: 80px;
    }
  }

  //md
  @media screen and (min-width: 900px) and (max-width: 1159px) {
    & .card {
      min-height: 200px;
      min-width: 200px;
      max-width: 300px;
    }

    & .logo {
      width: 80px;
    }
  }

  //lg
  @media screen and (min-width: 1200px) and (max-width: 1535px) {
  }

  //xxl
  @media screen and (min-width: 1920px) {
  }
`;

export default LoginContainerStyle;
