import styled from 'styled-components';
import border from '../../../styles/variables/border';
import color from '../../../styles/variables/color';

const IndexContainerStyle = styled.div`
  .dashboard-card {
    border-radius: ${border.BORDER_RADIUS_PX};
    width: 100%;
    background: ${color.WHITE_COLOR};
    min-height: 300px;
    margin-top: 25px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    box-sizing: border-box;
  }

  .header {
    padding: 25px;
    border-bottom: solid thin ${color.GRAY_COLOR_2};
  }
`;

export default IndexContainerStyle;
