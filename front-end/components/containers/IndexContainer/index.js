import React from 'react';
import IndexContainerStyle from './style';

function IndexContainer() {
  return (
    <IndexContainerStyle>
      <div className="content">
        <div className="dashboard-card">
          <div className="header">บัญชีของฉัน</div>
        </div>

        <div className="dashboard-card"></div>

      </div>
    </IndexContainerStyle>
  );
}

export default IndexContainer;
