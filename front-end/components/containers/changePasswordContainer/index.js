import { Button, TextField } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeAlert } from '../../../redux/globalRedux/action';
import { fetchingChangePassword } from '../../../redux/memberRedux/action';
import AlertModal from '../../shared/AlertModal';
import LoadingDialog from '../../shared/loading';
import ChangePasswordContainerStyle from './style';

const ChangePasswordContainer = React.forwardRef((props, ref) => {
  const initial = {
    cardId: '',
    newPassword: '',
    confirmPassword: '',
  };
  const dispatch = useDispatch();
  const router = useRouter();
  const [changePassword, setChangePassword] = useState(initial);
  const [error, setError] = useState(initial);
  const isLoading = useSelector((state) => state.global.options.isLoading);
  const alert = useSelector((state) => state.global.options.alert);

  const handleChange = (event) => {
    let name = event.target.name;
    let temp = { ...changePassword };
    temp[name] = event.target.value;
    setChangePassword({ ...temp });
  };

  const validate = () => {
    let err = {};
    if (changePassword.cardId === '') {
      //changePassword.length !== 13
      err.cardId = 'กรุณาระบุรหัสบัตรประชน 13 หลัก';
      err.isError = true;
    }

    if (changePassword.newPassword === '') {
      err.newPassword = 'กรุณากรอกรหัสผ่านใหม่ของท่าน';
      err.isError = true;
    }

    if (changePassword.newPassword !== changePassword.confirmPassword) {
      err.confirmPassword = 'กรุณากรอกยืนยันรหัสผ่านใหม่ให้ตรงกัน';
      err.isError = true;
    }

    if (changePassword.confirmPassword === '') {
      err.confirmPassword = 'กรุณากรอกยืนยันรหัสผ่านใหม่ของท่าน';
      err.isError = true;
    }

    setError({ isSubmit: true, ...err });
    return { isSubmit: true, ...err };
  };

  const handleSubmit = (e) => {
    if (!validate().isError) {
      changePassword.memberId = localStorage.getItem('memberId');
      dispatch(fetchingChangePassword(changePassword));
    }
    e.preventDefault();
  };

  const closeAlertModal = () => {
    dispatch(closeAlert());
    if (alert.success) {
      router.replace('/');
    }
  };
  return (
    <ChangePasswordContainerStyle>
      <LoadingDialog open={isLoading} />
      <AlertModal open={alert.open} onClose={() => closeAlertModal()} success={alert.success} error={!alert.success} subject={alert.subject} message={alert.message} />
      <div className="card">
        <div className="card-header">
          <div>เปลี่ยนรหัสผ่าน</div>
          <img className="logo" src="/images/logo.png" />
        </div>
        <div className="card-detail">
          <form onSubmit={handleSubmit}>
            <div className="error-msg">{error.isSubmit && error.isError && error.cardId}</div>
            <TextField name="cardId" onChange={handleChange} value={changePassword.cardId} label="รหัสบัตรประชาชน" className="input-space" variant="outlined" fullWidth />

            <div className="error-msg">{error.isSubmit && error.isError && error.newPassword}</div>
            <TextField
              name="newPassword"
              onChange={handleChange}
              type="password"
              value={changePassword.newPassword}
              label="รหัสผ่านใหม่"
              className="input-space"
              variant="outlined"
              fullWidth
            />

            <div className="error-msg">{error.isSubmit && error.isError && error.confirmPassword}</div>
            <TextField
              name="confirmPassword"
              onChange={handleChange}
              value={changePassword.confirmPassword}
              label="ยืนยันรหัสผ่าน"
              type="password"
              className="input-space"
              variant="outlined"
              fullWidth
            />

            <Button type="submit" className="btn-button" variant="contained">
              เปลี่ยนรหัสผ่าน
            </Button>
          </form>
        </div>
      </div>
    </ChangePasswordContainerStyle>
  );
});

export default ChangePasswordContainer;
