import styled from 'styled-components';
import border from '../../../styles/variables/border';
import color from '../../../styles/variables/color';

const MemberContainerStyle = styled.div`
  .card {
    border-radius: ${border.BORDER_RADIUS_PX};
    width: 100%;
    background: ${color.WHITE_COLOR};
    min-height: 300px;
    margin-top: 10px;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    box-sizing: border-box;
  }

  .console-manage {
    padding-top: 10px;
    text-align: end;
  }

  .row-wrapper {
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    align-content: center;
    justify-content: center;
    align-items: start;
  }

  .card-header {
    border-bottom: solid thin ${color.GRAY_COLOR_2};
    padding: 10px;

    & .row-wrapper {
      justify-content: center;
    }
  }

  .card-content {
    padding: 10px;

    & .row-wrapper {
      justify-content: start;
    }
  }

  .account-item {
    display: flex;
    align-content: flex-start;
  }

  .root-account {
    display: flex;
  }

  //xs
  @media screen and (max-width: 596px) {
  }

  //sm
  @media screen and (min-width: 600px) and (max-width: 899px) {
  }

  //md
  @media screen and (min-width: 900px) and (max-width: 1159px) {
  }

  //lg
  @media screen and (min-width: 1200px) and (max-width: 1535px) {
  }

  //xxl
  @media screen and (min-width: 1920px) {
  }
`;

export default MemberContainerStyle;
