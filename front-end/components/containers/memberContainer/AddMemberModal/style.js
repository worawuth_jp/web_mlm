import styled from 'styled-components';
import color from '../../../../styles/variables/color';

const AddMemberModalStyle = styled.div`
  .red-text {
    font-size: 14px;
    color: red !important;
  }

  .header-title {
    font-size: 24px !important;
    font-weight: 800 !important;
  }

  textarea {
    width: 100%;
    margin-top: 10px;
    padding: 10px;
    font-size: 18px;
    border-color: ${color.GRAY_COLOR_3};
  }

  .error-msg {
    color: red;
    font-size: 12px;
  }
`;

export default AddMemberModalStyle;
