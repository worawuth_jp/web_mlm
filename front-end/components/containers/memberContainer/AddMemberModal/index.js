import React, { useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextareaAutosize,
  TextField,
} from '@mui/material';
import AddMemberModalStyle from './style';
import DatePickerInput from '../../../shared/DatePicker';
import * as constants from '../../../../constants/memberConstants';
import AlertModal from '../../../shared/AlertModal';
import { useSelector, useDispatch } from 'react-redux';
import { closeAlert } from '../../../../redux/globalRedux/action';
import { createMemberUpdateTeam, fetchingMemberUnderTeam } from '../../../../redux/memberRedux/action';
import { useRouter } from 'next/router';

const AddMemberModal = React.forwardRef((props, ref) => {
  const initial = {
    cardId: '',
    memberPhone: '',
    firstName: '',
    lastName: '',
    birthDate: new Date(),
    bank: 0,
    bankAccount: '',
    address: '',
    refId: 0,
  };

  const dispatch = useDispatch();
  const router = useRouter();

  const [error, setError] = useState({ ...initial });
  const alert = useSelector((state) => state.global.options.alert);

  const handleChange = (event) => {
    let name = event.target.name;
    let temp = { ...addMemberModal };
    temp[name] = event.target.value;
    setAddMember({ ...temp });
  };

  const validate = () => {
    let err = {};
    if (addMemberModal.cardId === '') {
      err.cardId = 'กรุณาใส่เลขบัตรประชาชน';
      err.isError = true;
    }

    if (addMemberModal.memberPhone === '') {
      err.memberPhone = 'กรุณาใส่เบอร์โทรศัพท์';
      err.isError = true;
    }

    if (addMemberModal.firstName === '') {
      err.firstName = 'กรุณาใส่ชื่อจริง';
      err.isError = true;
    }

    if (addMemberModal.lastName === '') {
      err.lastName = 'กรุณาใส่ชื่อจริง';
      err.isError = true;
    }

    // if (addMemberModal.birthDate === '') {
    //   err.birthDate = 'กรุณาใส่วันเดือนปีเกิด';
    //   err.isError = true;
    // }

    // if (addMemberModal.bank === 0) {
    //   err.bank = 'กรุณาเลือกธนาคาร';
    //   err.isError = true;
    // }

    // if (addMemberModal.bankAccount === '') {
    //   err.bankAccount = 'กรุณาใส่เลขบัญชีธนาคาร';
    //   err.isError = true;
    // }

    // if (addMemberModal.address === '') {
    //   err.address = 'กรุณาใส่ที่อยู่';
    //   err.isError = true;
    // }

    setError({ isSubmit: true, ...err });

    return { isSubmit: true, ...err };
  };

  const handleSubmit = (e) => {
    let data = addMemberModal;
    data.refId = localStorage.getItem('memberId');
    const validateInput = validate();
    e.preventDefault();
    if (validateInput.isError) {
      return;
    }
    dispatch(createMemberUpdateTeam(data));
  };

  const closeAlertModal = () => {
    dispatch(closeAlert());
    if (alert.success) {
      dispatch(fetchingMemberUnderTeam(localStorage.getItem('memberId')));
      props.onClose();
    }
  };
  const [addMemberModal, setAddMember] = useState(initial);
  return (
    <Dialog open={props.open} onClose={props.onClose} fullWidth>
      <AlertModal open={alert.open} subject={alert.subject} message={alert.message} onClose={() => closeAlertModal()} success={alert.success} error={!alert.success} />
      <AddMemberModalStyle>
        <form onSubmit={handleSubmit}>
          <DialogTitle>
            <div className="header-title">เพิ่มสายงาน</div>
          </DialogTitle>
          <DialogContent>
            <span className="red-text">**กรุณากรอกข้อมูลให้ครบถ้วน**</span>
            <TextField
              autoFocus
              margin="dense"
              value={addMemberModal.cardId}
              onChange={handleChange}
              name="cardId"
              size="medium"
              label="รหัสบัตรประชาชน"
              inputProps={{ maxLength: 13 }}
              required
              type="text"
              fullWidth
              variant="outlined"
            />
            <div className="error-msg">{error.isSubmit && error.isError && error.cardId}</div>
            <TextField
              autoFocus
              margin="dense"
              value={addMemberModal.memberPhone}
              onChange={handleChange}
              name="memberPhone"
              size="medium"
              required
              label="เบอร์โทรศัพท์"
              type="text"
              inputProps={{ maxLength: 10 }}
              fullWidth
              variant="outlined"
            />
            <div className="error-msg">{error.isSubmit && error.isError && error.memberPhone}</div>
            <TextField
              autoFocus
              margin="dense"
              value={addMemberModal.firstName}
              onChange={handleChange}
              name="firstName"
              size="medium"
              label="ชื่อจริง"
              type="text"
              required
              fullWidth
              variant="outlined"
            />
            <div className="error-msg">{error.isSubmit && error.isError && error.firstName}</div>
            <TextField
              autoFocus
              margin="dense"
              value={addMemberModal.lastName}
              onChange={handleChange}
              name="lastName"
              size="medium"
              label="นามสกุล"
              required
              type="text"
              fullWidth
              variant="outlined"
            />
            <div className="error-msg">{error.isSubmit && error.isError && error.lastName}</div>
            <DatePickerInput
              value={addMemberModal.birthDate}
              setValue={(value) => {
                setAddMember((prev) => ({ ...prev, birthDate: value }));
              }}
            />
            <div className="error-msg">{error.isSubmit && error.isError && error.birthDate}</div>

            <FormControl fullWidth style={{ marginTop: 10 }}>
              <InputLabel id="demo-simple-select-label">ธนาคาร</InputLabel>
              <Select labelId="demo-simple-select-label" id="demo-simple-select" value={addMemberModal.bank} label="ธนาคาร" name="bank" onChange={handleChange}>
                <MenuItem key={`BANK_ID_${0}_${0}`} value={0}>
                  ธนาคารทั้งหมด
                </MenuItem>
                {constants.BANK_LIST.map((item, index) => (
                  <MenuItem key={`BANK_ID_${item.bankId}_${index}`} value={item.bankId}>
                    {item.bankName}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <div className="error-msg">{error.isSubmit && error.isError && error.bank}</div>

            <TextField
              autoFocus
              margin="dense"
              value={addMemberModal.bankAccount}
              onChange={handleChange}
              name="bankAccount"
              size="medium"
              label="เลขบัญชี"
              type="text"
              fullWidth
              variant="outlined"
            />
            <div className="error-msg">{error.isSubmit && error.isError && error.bankAccount}</div>

            <TextareaAutosize
              value={addMemberModal.address}
              onChange={handleChange}
              name="address"
              label="ที่อยู่"
              placeholder="ที่อยู่"
              minRows={4}
              maxRows={4}></TextareaAutosize>
            <div className="error-msg">{error.isSubmit && error.isError && error.address}</div>
          </DialogContent>
          <DialogActions>
            <Button onClick={props.onClose}>ยกเลิก</Button>
            <Button type="submit">บันทึก</Button>
          </DialogActions>
        </form>
      </AddMemberModalStyle>
    </Dialog>
  );
});

export default AddMemberModal;
