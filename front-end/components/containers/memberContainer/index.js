import React, { useEffect, useRef, useState } from 'react';
import Account from '../../shared/account';
import { Button } from '@mui/material';
import MemberContainerStyle from './style';
import * as constants from '../../../constants/memberConstants';
import Link from 'next/link';
import AddMemberModal from './AddMemberModal';
import AlertModal from '../../shared/AlertModal';
import { useSelector, useDispatch } from 'react-redux';
import LoadingDialog from '../../shared/loading';
import { closeAlert } from '../../../redux/globalRedux/action';
import { fetchingMemberUnderTeam } from '../../../redux/memberRedux/action';

const MemberContainer = React.forwardRef((props, ref) => {
  let cardContentRef = useRef({});
  const alert = useSelector((state) => state.global.options.alert);
  const isLoading = useSelector((state) => state.global.options.isLoading);
  const [open, setOpen] = useState(false);
  const [cardContentWidth, setCardContentWidth] = useState(0);
  const dispatch = useDispatch();
  let memberInfo = useSelector((state) => state.member.memberInfo);
  let memberLogin = useSelector((state) => state.member.memberLogin);
  let memberUnderTeam = useSelector((state) => state.member.memberUnderTeam);

  const closeAlertModal = () => {
    dispatch(closeAlert());
    if (alert.success) {
    }
  };
  useEffect(() => {
    if (props.memberId) {
      dispatch(fetchingMemberUnderTeam(props.memberId));
    }

    if (cardContentRef.current) {
      setCardContentWidth(cardContentRef.current.offsetWidth);
    }
  }, [props.memberId, cardContentRef, cardContentRef.current, cardContentWidth]);
  return (
    <MemberContainerStyle>
      <div className="content">
        <LoadingDialog open={isLoading} />
        <AlertModal open={alert.open} subject={alert.subject} message={alert.message} onClose={() => closeAlertModal()} success={alert.success} error={!alert.success} />
        <div className="console-manage">
          <AddMemberModal open={open} onClose={() => setOpen(false)} />
          {memberLogin.memberId === memberInfo.memberId && (
            <Button variant="contained" className="btn-button" onClick={() => setOpen(true)}>
              เพิ่มสายงาน
            </Button>
          )}
        </div>
        <div className="card">
          <div className="card-header">
            <div className="row-wrapper">
              <Account
                key={`Root_1`}
                memberId={memberInfo.memberCode ? memberInfo.memberCode : ''}
                firstname={memberInfo.firstName ? memberInfo.firstName : ''}
                lastname={memberInfo.lastName ? memberInfo.lastName : ''}
                root={true}
              />
            </div>
          </div>

          <div className="card-content" ref={cardContentRef}>
            <div className="row-wrapper">
              {memberUnderTeam.map((item, index) => (
                <Link key={`MemberUnder_${index}`} href={`/member/${item.memberId}`}>
                  <div className="account-item">
                    <Account memberId={item.memberCode} firstname={item.firstName} lastname={item.lastName} cardWidth={cardContentWidth} />
                  </div>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
    </MemberContainerStyle>
  );
});
export default MemberContainer;
