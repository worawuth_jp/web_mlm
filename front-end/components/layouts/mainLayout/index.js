import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import useWindowSize from '../../../modules/windowSize';
import Navbar from '../../shared/navbar';
import Sidebar from '../../shared/sidebar';
import MainLayoutStyle from './style';
import { useSelector } from 'react-redux';

const MainLayout = React.forwardRef((props, ref) => {
  let size = useWindowSize();
  let navRef = useRef({});
  const isLogin = useSelector((state) => state.member.memberLogin.isLogin);
  const router = useRouter();
  const [navbarHeight, setNavbarHeight] = useState(0);

  useEffect(() => {
    if (!isLogin) {
      router.push('/login');
    }
    if (navRef.current) {
      setNavbarHeight(navRef.current.offsetHeight);
    }
  }, [size, navRef, navRef.current, navbarHeight, isLogin]);
  return (
    <MainLayoutStyle screenHeight={size.height} screenWidth={size.width} navHeight={navbarHeight}>
      <Navbar ref={navRef} active={props.page} />
      {/* <Sidebar screenHeight={size.height} /> */}
      <div className="content-wrapper">{props.children}</div>
    </MainLayoutStyle>
  );
});

export default MainLayout;
