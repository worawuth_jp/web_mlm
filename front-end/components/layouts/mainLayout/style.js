import styled from 'styled-components';
import color from '../../../styles/variables/color';

const MainLayoutStyle = styled.div`
  max-height: ${(props) => props.screenHeight}px;
  width: ${(props) => props.screenWidth}px;
  background: ${color.GRAY_COLOR_1} !important;
  position: relative;
  overflow: hidden;

  .content-wrapper {
    overflow-x: hidden;
    overflow-y: auto;
    margin-top: ${(props) => props.navHeight}px;
    height: calc(100vh - ${(props) => props.navHeight}px);
  }
`;

export default MainLayoutStyle;
