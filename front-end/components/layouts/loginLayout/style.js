import styled from 'styled-components';

const LoginLayoutStyle = styled.div`
  background-image: linear-gradient(to right, #434343 0%, black 100%);
  min-height: ${(props) => props.screenHeight}px;
  max-height: ${(props) => props.screenHeight}px;
  width: ${(props) => props.screenWidth}px;
  overflow: hidden;
  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;
`;
export default LoginLayoutStyle;
