import React, { useEffect } from 'react';
import useWindowSize from '../../../modules/windowSize';
import LoginLayoutStyle from './style';

const LoginLayout = React.forwardRef((props, ref) => {
  let size = useWindowSize();
  useEffect(() => {
    console.debug(size);
  }, [size]);
  return (
    <LoginLayoutStyle screenHeight={size.height} screenWidth={size.width}>
      {props.children}
    </LoginLayoutStyle>
  );
});

export default LoginLayout;
