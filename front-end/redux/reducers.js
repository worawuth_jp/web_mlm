import GlobalReducer from './globalRedux/reducer';
import MemberReducer from './memberRedux/reducer';
const { combineReducers } = require('redux');

const rootReducers = combineReducers({
  global: GlobalReducer,
  member: MemberReducer,
});

export default rootReducers;
