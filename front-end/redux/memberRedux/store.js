const state = {
  constants: {},
  memberLogin: {
    memberId: 0,
    memberCode: '',
    memberPhone: '',
    cardId: '',
    firstName: '',
    lastName: '',
    isLogin: false,
    token: '',
  },
  memberInfo: {},
  memberUnderTeam: [],
};
export default state;
