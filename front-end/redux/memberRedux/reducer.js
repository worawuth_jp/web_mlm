import * as actionType from './actionType';
import initialState from './store';

const MemberReducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case actionType.GET_LOGIN_INFO_SUCCESS: {
      return gettingLoginInfoSuccess(state, action);
    }
    case actionType.GET_MEMBER_UNDER_TEAM: {
      return gettingMemberUnderTeam(state, action);
    }
    default: {
      return { ...state };
    }
  }
};
export default MemberReducer;

const gettingLoginInfoSuccess = (state, action) => {
  let loginInfoMapper = loginMapperInfo(action.payload.data);
  return {
    ...state,
    memberLogin: {
      ...state.memberLogin,
      ...loginInfoMapper,
    },
  };
};

const loginMapperInfo = (data) => {
  return {
    memberId: data.user.member_id,
    memberCode: data.user.member_code,
    memberPhone: data.user.member_phone,
    cardId: data.user.card_id,
    firstName: data.user.first_name,
    lastName: data.user.last_name,
    isLogin: data.isLogin,
    token: data.token,
  };
};

const gettingMemberUnderTeam = (state, action) => {
  let memberInfoMapper = dataMemberInfoMapper(action.payload.data);
  let memberUnderTeamList = dataMemberUnderTeamList(action.payload.data.memberLists);
  return {
    ...state,
    memberInfo: memberInfoMapper,
    memberUnderTeam: memberUnderTeamList,
  };
};

const dataMemberInfoMapper = (data) => {
  return {
    level: data.level,
    memberId: data.memberId,
    memberCode: data.memberCode,
    cardId: data.cardId,
    firstName: data.firstName,
    lastName: data.lastName,
    birthDate: data.birthDate,
    memberPhone: data.memberPhone,
    bankName: data.bankName,
    bankAccount: data.bankAccount,
    memberLists: data.memberLists,
  };
};

const dataMemberUnderTeamList = (data) => {
  return data.map((item, index) => ({
    level: item.level,
    memberId: item.memberId,
    memberCode: item.memberCode,
    cardId: item.cardId,
    firstName: item.firstName,
    lastName: item.lastName,
    birthDate: item.birthDate,
    memberPhone: item.memberPhone,
    bankName: item.bankName,
    bankAccount: item.bankAccount,
    memberLists: item.memberLists,
  }));
};
