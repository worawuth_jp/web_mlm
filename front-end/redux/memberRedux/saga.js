import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import * as actionType from './actionType';
import CoreService from '../../services/coreService';
import { getLoginInfo, gettingMemberUnderTeam } from './action';
import { callApiFail, callApiSuccess, setLoadingOff, setLoadingOn } from '../globalRedux/action';
import ErrorModel from '../../models/errorModel';
import { errorMessage } from '../../constants/errorConstants';
let services = new CoreService();
// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchLogin(action) {
  try {
    yield put(setLoadingOn());
    const result = yield services.login(action.payload.data.username, action.payload.data.password);
    if (result.isLogin) {
      yield put(getLoginInfo(result));
      localStorage.setItem('token', result.token);
      localStorage.setItem('memberId', result.user.member_id);
      localStorage.setItem('memberCode', result.user.member_code);
      //yield put(callApiSuccess());
    }else{
      yield put(callApiFail(new ErrorModel(400, result.msg)));
    }
    yield put(setLoadingOff());
  } catch (e) {
    yield put(setLoadingOff());
    if (e.code) {
      yield put(callApiFail(e));
    } else {
      yield put(callApiFail(new ErrorModel(e.statusCode, e.statusMessage)));
    }
  }
}

function* fetchMemberUnderTeam(action) {
  try {
    yield put(setLoadingOn());
    const result = yield services.getAllMemberUnderTeam(action.payload.memberId);
    if (result) {
      yield put(gettingMemberUnderTeam(result));
      //yield put(callApiSuccess());
    }
    yield put(setLoadingOff());
  } catch (e) {
    yield put(setLoadingOff());
    if (e.code) {
      yield put(callApiFail(e));
    } else {
      yield put(callApiFail(new ErrorModel(e.statusCode, e.statusMessage)));
    }
  }
}

function* fetchCreateMemberUnderTeam(action) {
  try {
    yield put(setLoadingOn());
    const result = yield services.createMember(action.payload.data);
    if (result.createSuccess) {
      yield put(callApiSuccess());
    } else {
      yield put(callApiFail(new ErrorModel(400, result.msg)));
    }
    yield put(setLoadingOff());
  } catch (e) {
    console.error('error', e);
    yield put(setLoadingOff());
    if (e.code) {
      yield put(callApiFail(e));
    } else {
      yield put(callApiFail(new ErrorModel(e.statusCode, e.statusMessage)));
    }
  }
}

function* fetchingChangePassword(action) {
  try {
    yield put(setLoadingOn());
    const result = yield services.changePassword(action.payload.data);
    if (result.data.affectedRows) {
      yield put(callApiSuccess());
    } else {
      yield put(callApiFail(new ErrorModel(400, result.msg)));
    }
    yield put(setLoadingOff());
  } catch (e) {
    console.error('error', e);
    yield put(setLoadingOff());
    if (e.code) {
      yield put(callApiFail(e));
    } else {
      yield put(callApiFail(new ErrorModel(e.statusCode, e.statusMessage)));
    }
  }
}

function* memberSaga() {
  yield takeEvery(actionType.FETCH_LOGIN, fetchLogin);
  yield takeEvery(actionType.FETCH_MEMBER_UNDER_TEAM, fetchMemberUnderTeam);
  yield takeEvery(actionType.FETCH_CREATE_MEMBER_UNDER_TEAM, fetchCreateMemberUnderTeam);
  yield takeEvery(actionType.FETCH_CHANGE_PASSWORD, fetchingChangePassword);
}

export default memberSaga;
