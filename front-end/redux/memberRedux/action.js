import * as actionType from './actionType';

export const fetchLogin = (username, password) => {
  return {
    type: actionType.FETCH_LOGIN,
    payload: {
      data: {
        username,
        password,
      },
    },
  };
};

export const getLoginInfo = (data) => ({
  type: actionType.GET_LOGIN_INFO_SUCCESS,
  payload: {
    data,
  },
});

export const fetchingMemberUnderTeam = (memberId) => ({
  type: actionType.FETCH_MEMBER_UNDER_TEAM,
  payload: {
    memberId: memberId,
  },
});

export const gettingMemberUnderTeam = (data) => ({
  type: actionType.GET_MEMBER_UNDER_TEAM,
  payload: {
    data,
  },
});

export const createMemberUpdateTeam = (data) => ({
  type: actionType.FETCH_CREATE_MEMBER_UNDER_TEAM,
  payload: { data },
});

export const fetchingChangePassword = (data) => ({
  type: actionType.FETCH_CHANGE_PASSWORD,
  payload: { data },
});
