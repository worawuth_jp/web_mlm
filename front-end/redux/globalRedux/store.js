const state = {
  options: {
    isLoading: false,
    alert: {
      open: false,
      success: false,
      message: '',
      code: '',
      subject: '',
    },
  },
  constants: {},
};
export default state;
