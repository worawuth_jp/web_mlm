import { all } from 'redux-saga/effects';
import globalSaga from './globalRedux/saga';
import memberSaga from './memberRedux/saga';

export default function* rootSaga() {
  yield all([globalSaga(), memberSaga()]);
  // code after all-effect
}
