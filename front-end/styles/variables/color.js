export default {
  PRIMARY: 'linear-gradient(to right, #434343 0%, black 100%)',
  SECONDARY: '#EC1E0D',
  RED_COLOR: '#F94D3F',
  WHITE_COLOR: '#ffffff',
  GRAY_COLOR_1: '#F0F0F0',
  GRAY_COLOR_2: '#939090',
  GRAY_COLOR_3: '#545151',
};
