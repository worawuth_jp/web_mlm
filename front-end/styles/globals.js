import border from './variables/border';
import font from './variables/font';

const { default: styled, createGlobalStyle } = require('styled-components');

const GlobalStyle = createGlobalStyle`

html,
body {
  padding: 0;
  margin: 0;
  font-family: ${font.FONT} -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  font-size: ${font.FONT_SIZE_PX};
}

a {
  color: inherit;
  text-decoration: none;
}

* {
  box-sizing: border-box;
}

fieldset,input,select,textarea {
  border-radius: ${border.BORDER_RADIUS_PX} !important;
}

.input-space {
  margin-bottom: 10px !important;
}

.btn-button {
  border-radius: ${border.BORDER_RADIUS_PX} !important;
  padding: 5px 25px !important;
  font-size: ${font.FONT_SIZE_PX} !important;
}

.content {
  padding: 0 5% 5%;
}

//xs
@media screen and (max-width: 596px) {
    
}

//sm
@media screen and (min-width: 600px) and (max-width: 899px) {
    
}

//md
@media screen and (min-width: 900px) and (max-width: 1159px) {
    
}

//lg
@media screen and (min-width: 1200px) and (max-width: 1535px) {
    
}

//xxl
@media screen and (min-width: 1920px) {
    
}
`;

export default GlobalStyle;
