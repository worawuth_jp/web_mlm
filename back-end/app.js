let express = require('express');
const app = express();
const cors = require('cors');
const cookieParser = require('cookie-parser');
const httpContext = require('express-http-context');
const path = require('path');
const { mapVersioning } = require('./middlewares/MapVersionMiddleWare');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const morgan = require('morgan');

//import router
const indexRouter = require('./routes/index');
const CommonResponse = require('./models/response/CommonResponse');
const MemberRouter = require('./routes/MemberRouter');

//end
const corsOption = {
    origin: true,
};

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(
    fileUpload({
        createParentPath: true,
    })
);
app.use(cors(corsOption));
app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({ limit: '100mb', extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static(path.join(__dirname,'public/uploads')))
app.use(morgan('dev'));
app.use(httpContext.middleware);

//middleware
app.use(mapVersioning);
//call routes
app.use('/', indexRouter);
app.use('/api', MemberRouter);
//handle 404
app.use((req, res, next) => {
    const commonResponse = new CommonResponse();
    commonResponse.statusCode = 404;
    commonResponse.statusMessage = 'Not Found';
    res.status(404).send(commonResponse);
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'dev' ? err : {};

    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
