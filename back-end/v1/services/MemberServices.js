const DBConnector = require('../../API/DBConnector');
const http = require('../../constants/http');
const ApplicationError = require('../../models/errors/ApplicationError');
const LogManager = require('../../utils/LogManager');
const MemberRepository = require('../repositories/MemberRepository');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const MemberRelationRepository = require('../repositories/MemberRelationRepository');
const DigitMemberRepository = require('../repositories/DigitMemberRepository');
const DigitMemberService = require('./DigitMemberServices');
const TransformUtils = require('../utils/TransformUtils');
const { connection } = require('mongoose');

let logger = LogManager.getLogger('MemberServices');

class MemberServices {
    async createMember(cardId, memberPhone, firstName, lastName, birthDate, bank, bankAccount, address, refId) {
        let connection = await DBConnector.getConnector();
        try {
            logger.debug('CARD 4 LAST', cardId.slice(cardId.length - 4, cardId.length));
            let encryptPassword = await new Promise((resolve, reject) => {
                bcrypt.genSalt(10, function (err, salt) {
                    if (err) reject(err);
                    bcrypt.hash(cardId.slice(cardId.length - 4, cardId.length), salt, function (err, hash) {
                        if (err) reject(err);
                        resolve(hash);
                    });
                });
            });

            let memberCode = await _generateMemberCode(connection);
            let params = [memberCode, cardId, memberPhone, encryptPassword, firstName, lastName, birthDate, bank ? bank : 18, bankAccount, address];
            let memberRepository = new MemberRepository(connection);
            let isCardIdExist = await memberRepository.isUserExist(cardId);
            let isPhoneExist = await memberRepository.isUserExist(memberPhone);

            if (isCardIdExist.isExist || isPhoneExist.isExist) {
                return {
                    createSuccess: false,
                    msg: 'รหัสบัตรประชาชน/เบอร์โทรศัพท์ มีในระบบแล้ว',
                };
            }

            await connection.beginTransaction();

            let insertResult = await memberRepository.createMember(params);
            let memberId = insertResult.data.insertId;
            let resultData = { createSuccess: insertResult.data.affectedRows > 0 };
            if (memberId) {
                resultData.memberId = memberId;
                if (refId) {
                    let memberRelationRepository = new MemberRelationRepository(connection);
                    let result = await memberRelationRepository.mappingRelation(memberId, refId);
                    logger.debug('INSERT MEMBER RELATION ', result);
                    resultData.isMapping = result.data.affectedRows > 0;
                }
            }

            await DBConnector.commit(connection);

            return resultData;
        } catch (error) {
            logger.error('MemberServices createMember() Exception :', error);

            await connection.rollback();
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            await DBConnector.releaseConnector(connection);
        }
    }

    async login(username, password) {
        let connection = await DBConnector.getConnector();
        try {
            let memberRepository = new MemberRepository(connection);
            let isUserExist = await memberRepository.isUserExist(username);
            if (!isUserExist.isExist) {
                return {
                    isLogin: false,
                    msg: 'ไม่พบ username ในระบบ',
                };
            }
            let user = isUserExist.data;
            let status = await new Promise((resolve, reject) => {
                bcrypt.compare(password, user.member_password, function (err, result) {
                    // result == true
                    if (err) reject(err);
                    resolve(result);
                });
            });

            if (!status) {
                return {
                    isLogin: false,
                    msg: 'username/password ไม่ถูกต้อง',
                };
            }

            var token = jwt.sign(
                {
                    data: {
                        memberId: user.member_id,
                        username: username,
                    },
                },
                process.env.SECRET_KEY + username,
                { expiresIn: process.env.EXPIRE_IN || '3h' }
            );

            return {
                isLogin: true,
                token: token,
                user: { ...user, member_password: undefined },
            };
        } catch (error) {
            logger.error('MemberServices login() Exception :', error);

            await connection.rollback();
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            DBConnector.releaseConnector(connection);
        }
    }

    async getAllMemberUnderTeam(memberId, search) {
        let connection = await DBConnector.getConnector();
        try {
            let memberLists = await _getChildMember(connection, memberId, 1, 2);
            let memberRepo = new MemberRepository(connection);
            let member = await memberRepo.findMemberInfo(memberId);
            if (search) {
                member.memberLists = await _searchMember(connection, memberId, search);
            } else {
                member.memberLists = memberLists;
            }

            return { level: 0, ...TransformUtils.toMemberModel(member) };
        } catch (error) {
            logger.error('MemberServices getAllMemberUnderTeam() Exception: ', error);
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            DBConnector.releaseConnector(connection);
        }
    }

    async generateMemberCode() {
        let connection = await DBConnector.getConnector();
        try {
            return await _generateMemberCode(connection);
        } catch (error) {
            logger.error('MemberServices generateMemberCode() Exception :', error);

            await connection.rollback();
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            await DBConnector.releaseConnector(connection);
        }
    }

    async changePassword(cardId, memberId, newPassword) {
        let connection = await DBConnector.getConnector();
        try {
            let memberRepository = new MemberRepository(connection);

            let isCardValid = await memberRepository.checkCardValid(memberId, cardId);
            if (!isCardValid) {
                throw new ApplicationError(http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_CODE, 'หมายเลขบัตรประชาชนไม่ถูกต้อง');
            }

            let encryptPassword = await new Promise((resolve, reject) => {
                bcrypt.genSalt(10, function (err, salt) {
                    if (err) reject(err);
                    bcrypt.hash(newPassword, salt, function (err, hash) {
                        if (err) reject(err);
                        resolve(hash);
                    });
                });
            });

            let result = await memberRepository.changePassword(memberId, encryptPassword);
            return { msg: 'เปลี่ยนรหัสผ่านสำเร็จ', data: result };
        } catch (error) {
            logger.error('MemberServices changePassword() Exception :', error);

            await connection.rollback();
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            await DBConnector.releaseConnector(connection);
        }
    }

    async editMember(memberId, cardId, memberPhone, firstName, lastName, birthDate, bank, bankAccount, address) {
        let connection = await DBConnector.getConnector();
        try {
            let memberRepo = new MemberRepository(connection);

            await connection.beginTransaction();
            let params = [cardId, memberPhone, firstName, lastName, birthDate, bank, bankAccount, address, memberId];
            let resultData = { editSuccess: true, match: 0 };
            let result = await memberRepo.editMember(params);

            logger.info('RESULT : ', result);

            if (result.data.affectedRows > 0) {
                resultData.match = result.data.affectedRows;
            }

            await DBConnector.commit(connection);

            return resultData;
        } catch (error) {
            logger.error('MemberServices editMember() Exception :', error);

            await connection.rollback();
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            await DBConnector.releaseConnector(connection);
        }
    }
}

module.exports = MemberServices;

let _generateMemberCode = async (connection) => {
    try {
        const alpha = Array.from(Array(26)).map((e, i) => i + 65);
        const alphabet = alpha.map((x) => String.fromCharCode(x));
        let prefixList = Array.from(Array(10)).map((e, i) => `${i}`);
        prefixList = [...prefixList, ...alphabet];

        let digitMemberService = new DigitMemberService();
        let digitMemberRepository = new DigitMemberRepository(connection);
        let digitMemberObj = await digitMemberService.getDigitMember();

        let prefix = digitMemberObj.prefix;
        let indexPrefix = prefixList.indexOf(prefix);

        let prefixNum = digitMemberObj.prefix_num;
        let prefixDigit = digitMemberObj.prefix_digits;
        let sequence = digitMemberObj.sequence;
        let sequenceDigit = digitMemberObj.sequence_digits;
        let result = '';
        if (sequence >= Math.pow(10, sequenceDigit) - 1) {
            prefixNum += 1;
            sequence = -1;
        }

        if (prefixNum > Math.pow(10, prefixDigit) - 1) {
            indexPrefix += 1;
            prefixNum = 1;
            sequence = -1;
        }
        logger.info('indexPrefix ', indexPrefix);

        if (indexPrefix >= prefixList.length) {
            return '';
        }

        result = '' + prefixList[indexPrefix] + _formatCodeDigit(prefixNum, prefixDigit) + _formatCodeDigit(sequence + 1, sequenceDigit);
        await digitMemberRepository.updatePrefix(prefixList[indexPrefix], digitMemberObj.digit_member_id);
        await digitMemberRepository.updatePrefixNum(prefixNum, digitMemberObj.digit_member_id);
        await digitMemberRepository.updateSequence(sequence + 1, digitMemberObj.digit_member_id);
        return result;
    } catch (error) {
        logger.error('MemberServices _generateMemberCode()', error);
        throw error;
    }
};

let _formatCodeDigit = (val, digit) => {
    let result = `${val}`;
    while (result.length < digit) {
        result = '0' + result;
    }

    return result;
};

let _getChildMember = async (connection, memberId, level, maxLevel = 1) => {
    if (level >= maxLevel) {
        return [];
    }
    try {
        let memberRelationRepo = new MemberRelationRepository(connection);
        let memberRepo = new MemberRepository(connection);
        let totalChild = await memberRelationRepo.countChildMember(memberId);

        if (totalChild <= 0) {
            return [];
        }

        let memberList = await memberRepo.findAllMemberUnderTeam(memberId);
        let memberModelLists = await Promise.all(
            memberList.map(async (item) => {
                item.memberLists = await _getChildMember(connection, item.member_id, level + 1);
                return { level: level, ...TransformUtils.toMemberModel(item) };
            })
        );
        // for await (const member of memberList) {
        //     member.memberLists = await _getChildMember(connection, member.member_id);
        // }
        return memberModelLists;
    } catch (error) {
        logger.error('MemberServices _generateMemberCode()', error);
        throw error;
    }
};

let _searchMember = async (connection, memberId, searchText) => {
    try {
        let memberRepo = new MemberRepository(connection);
        let likeText = `%${searchText}%`;
        let params = [likeText, likeText, likeText, likeText, likeText];
        let resultMemberSearch = await memberRepo.searchMember(params);

        let memberLists = resultMemberSearch.map((member) => TransformUtils.toMemberModel(member));
        logger.info('memberLists : ', memberLists);
        let resultMember = [];
        for await (let member of memberLists) {
            if (await _isChildMember(connection, memberId, member.memberId)) {
                resultMember.push(member);
            }
        }
        return resultMember;
    } catch (error) {
        logger.error('MemberServices _searchMember()', error);
        throw error;
    }
};

let _isChildMember = async (connection, parentMember, childMember) => {
    try {
        let memberRelationRepo = new MemberRelationRepository(connection);
        let refId = 0;
        let isChildMember = false;
        let memberId = childMember;
        do {
            refId = await memberRelationRepo.findRefId(memberId);
            logger.info('REF ID : ', refId);
            logger.info('MEMBER PARENT ID : ', parentMember);
            if (refId === parseInt(parentMember)) {
                isChildMember = true;
                break;
            } else {
                memberId = refId;
            }
        } while (refId !== parseInt(parentMember) && refId !== null);

        return isChildMember;
    } catch (error) {
        logger.error('MemberServices _searchMember()', error);
        throw error;
    }
};
