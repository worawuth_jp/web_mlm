const DBConnector = require('../../API/DBConnector');
const http = require('../../constants/http');
const ApplicationError = require('../../models/errors/ApplicationError');
const LogManager = require('../../utils/LogManager');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const DigitMemberRepository = require('../repositories/DigitMemberRepository');

let logger = LogManager.getLogger('MemberServices');

class DigitMemberService {
    async getDigitMember() {
        let connection = await DBConnector.getConnector();
        try {
            let digitMemberRepository = new DigitMemberRepository(connection);
            let result = await digitMemberRepository.getDigitMember();
            return result.data[0];
        } catch (error) {
            logger.error('DigitMemberService getDigitMember() Exception :', error);

            await connection.rollback();
            if (error.http) {
                throw new ApplicationError(error.http, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE.msg, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        } finally {
            await DBConnector.releaseConnector(connection);
        }
    }
}

module.exports = DigitMemberService;
