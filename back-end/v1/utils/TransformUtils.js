const MemberModel = require('../models/responses/memberModel');
class TransformUtils {
    toMemberModel(data) {
        let memberModel = new MemberModel();
        memberModel.memberId = data.member_id;
        memberModel.memberCode = data.member_code;
        memberModel.cardId = data.card_id;
        memberModel.firstName = data.first_name;
        memberModel.lastName = data.last_name;
        memberModel.birthDate = data.birth_date;
        memberModel.memberPhone = data.member_phone;
        memberModel.bankName = data.bank_name;
        memberModel.bankAccount = data.bank_account;
        memberModel.address = data.address;
        memberModel.memberLists = data.memberLists;

        return memberModel;
    }
}
module.exports = new TransformUtils();
