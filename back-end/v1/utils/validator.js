class Validator {
    isEmptyUndefinedNullAndZero(val) {
        return val === null || val === void 0 || val === 0 || val === '';
    }
}

module.exports = new Validator();
