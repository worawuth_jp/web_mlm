const http = require('../../constants/http');
const ApplicationError = require('../../models/errors/ApplicationError');
const LogManager = require('../../utils/LogManager');
const httpContext = require('express-http-context');
const Validator = require('../utils/validator');
const MemberServices = require('../services/MemberServices');
const TransformResponseUtils = require('../../utils/TransformResponseUtils');

const logger = LogManager.getLogger('MemberController');

class DigitMemberController {
    async generateDigitMember(req) {
        try {
            let result = await new MemberServices().generateMemberCode();
            return TransformResponseUtils.toResponseSuccessWithData(result);
        } catch (error) {
            logger.error('DigitMemberController generateDigitMember() Exception: ', error);
            if (error.code) {
                throw new ApplicationError(error.code, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        }
    }
}
module.exports = DigitMemberController;
