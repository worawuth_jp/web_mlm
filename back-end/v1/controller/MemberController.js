const http = require('../../constants/http');
const ApplicationError = require('../../models/errors/ApplicationError');
const LogManager = require('../../utils/LogManager');
const httpContext = require('express-http-context');
const Validator = require('../utils/validator');
const MemberServices = require('../services/MemberServices');
const TransformResponseUtils = require('../../utils/TransformResponseUtils');

const logger = LogManager.getLogger('MemberController');

class MemberController {
    async login(body) {
        try {
            console.log(body);
            if (body === undefined || Object.keys(body).length == 0 || !body.username || !body.password || body.username === '' || body.password === '' || body.username === undefined || body.password === undefined) {
                throw new ApplicationError(http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_MSG);
            }

            let resultSuccess = await new MemberServices().login(body.username, body.password);
            return TransformResponseUtils.toResponseSuccessWithData(resultSuccess);
        } catch (error) {
            logger.error('MemberController login() Exception: ', error);
            if (error.code) {
                throw new ApplicationError(error.code, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        }
    }

    async createMember(req) {
        try {
            let cardId = req.cardId;
            let memberPhone = req.memberPhone;
            let firstName = req.firstName;
            let lastName = req.lastName;
            let birthDate = req.birthDate;
            let bank = req.bank;
            let bankAccount = req.bankAccount;
            let address = req.address;
            let refId = req.refId;
            let cardIdValidate = Validator.isEmptyUndefinedNullAndZero(cardId);
            let memberPhoneValidate = Validator.isEmptyUndefinedNullAndZero(memberPhone);
            let firstNameValidate = Validator.isEmptyUndefinedNullAndZero(firstName);
            let lastNameValidate = Validator.isEmptyUndefinedNullAndZero(lastName);
            // let birthDateValidate = Validator.isEmptyUndefinedNullAndZero(birthDate);
            // let bankValidate = Validator.isEmptyUndefinedNullAndZero(bank);
            // let bankAccountValidate = Validator.isEmptyUndefinedNullAndZero(bankAccount);
            // let addressValidate = Validator.isEmptyUndefinedNullAndZero(address);

            if (cardIdValidate || memberPhoneValidate || firstNameValidate || lastNameValidate) {
                //|| birthDateValidate || bankValidate || bankAccountValidate || addressValidate
                throw new ApplicationError(http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_MSG, []);
            }

            let result = await new MemberServices().createMember(cardId, memberPhone, firstName, lastName, birthDate, bank, bankAccount, address, refId);
            return TransformResponseUtils.toResponseSuccessWithData(result);
        } catch (error) {
            logger.error('MemberController createMember() Exception: ', error);
            if (error.code) {
                throw new ApplicationError(error.code, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        }
    }

    async editMember(req) {
        try {
            let memberId = req.memberId;
            let cardId = req.cardId;
            let memberPhone = req.memberPhone;
            let firstName = req.firstName;
            let lastName = req.lastName;
            let birthDate = req.birthDate;
            let bank = req.bank;
            let bankAccount = req.bankAccount;
            let address = req.address;
            let memberIdValidate = Validator.isEmptyUndefinedNullAndZero(memberId);
            let cardIdValidate = Validator.isEmptyUndefinedNullAndZero(cardId);
            let memberPhoneValidate = Validator.isEmptyUndefinedNullAndZero(memberPhone);
            let firstNameValidate = Validator.isEmptyUndefinedNullAndZero(firstName);
            let lastNameValidate = Validator.isEmptyUndefinedNullAndZero(lastName);

            if (memberIdValidate || cardIdValidate || memberPhoneValidate || firstNameValidate || lastNameValidate) {
                throw new ApplicationError(http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_MSG, []);
            }

            let result = await new MemberServices().editMember(memberId, cardId, memberPhone, firstName, lastName, birthDate, bank, bankAccount, address);
            return TransformResponseUtils.toResponseSuccessWithData(result);
        } catch (error) {
            logger.error('MemberController editMember() Exception: ', error);
            if (error.code) {
                throw new ApplicationError(error.code, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        }
    }

    async getMemberUnderTeam(params) {
        try {
            let memberId = params.memberId;
            let search = params.search;
            if (memberId === void 0 || memberId === null || memberId === '' || memberId === 0) {
                throw new ApplicationError(http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_MSG, void 0);
            }
            let resultSuccess = await new MemberServices().getAllMemberUnderTeam(memberId, search);
            return TransformResponseUtils.toResponseSuccessWithData(resultSuccess);
        } catch (error) {
            logger.error('MemberController getMemberUnderTeam() Exception: ', error);
            if (error.code) {
                throw new ApplicationError(error.code, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        }
    }

    async changePassword(params) {
        try {
            let memberId = params.memberId;
            let newPassword = params.newPassword;
            let cardId = params.cardId;
            if (
                memberId === void 0 ||
                memberId === null ||
                memberId === '' ||
                memberId === 0 ||
                newPassword === void 0 ||
                newPassword === '' ||
                newPassword === null ||
                cardId === void 0 ||
                cardId === '' ||
                cardId === null
            ) {
                throw new ApplicationError(http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_CODE, http.HTTP_CLIENT_ERROR_MSG, void 0);
            }
            let resultSuccess = await new MemberServices().changePassword(cardId, memberId, newPassword);
            return TransformResponseUtils.toResponseSuccessWithData(resultSuccess);
        } catch (error) {
            logger.error('MemberController changePassword() Exception: ', error);
            if (error.code) {
                throw new ApplicationError(error.code, error.code, error.msg, error.stack);
            } else {
                throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
            }
        }
    }
}
module.exports = MemberController;
