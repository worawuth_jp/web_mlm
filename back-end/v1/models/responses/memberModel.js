class MemberModel {
    get getMemberId() {
        return this.memberId;
    }
    set setMemberId(memberId) {
        this.memberId = memberId;
    }

    get getMemberCode() {
        return this.memberCode;
    }
    set setMemberCode(memberCode) {
        this.memberCode = memberCode;
    }

    get getFirstName() {
        return this.firstName;
    }
    set setFirstName(firstName) {
        this.firstName = firstName;
    }

    get getLastName() {
        return this.lastName;
    }
    set setLastName(lastName) {
        this.lastName = lastName;
    }

    get getCardId() {
        return this.cardId;
    }
    set setCardId(cardId) {
        this.cardId = cardId;
    }

    get getMemberPhone() {
        return this.memberPhone;
    }
    set setMemberPhone(memberPhone) {
        this.memberPhone = memberPhone;
    }

    get getBirthDate() {
        return this.birthDate;
    }
    set setBirthDate(birthDate) {
        this.birthDate = birthDate;
    }

    get getBankName() {
        return this.bankName;
    }
    set setBankName(bankName) {
        this.bankName = bankName;
    }

    get getBankAccount() {
        return this.bankAccount;
    }
    set setBankAccount(bankAccount) {
        this.bankAccount = bankAccount;
    }

    get getAddress() {
        return this.address;
    }
    set setAddress(address) {
        this.address = address;
    }

    get getRefId() {
        return this.refId;
    }
    set setRefId(refId) {
        this.refId = refId;
    }

    get getMemberLists() {
        return this.memberLists;
    }
    set setMemberLists(memberLists) {
        this.memberLists = memberLists;
    }
}

module.exports = MemberModel;
