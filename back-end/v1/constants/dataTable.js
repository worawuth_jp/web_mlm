module.exports = {
    TABLE_BANKS: 'banks',
    TABLE_DIGIT_MEMBER: 'digit_member',
    TABLE_MEMBERS: 'members',
    TABLE_EXTRA_COMMISSION: 'extra_commission',
    TABLE_MEMBER_RELATION: 'member_relation',
    TABLE_ORDERS: 'orders',
    TABLE_ORDER_DETAILS: 'order_details',
    TABLE_PRODUCTS: 'products',
    TABLE_WALLETS: 'wallets',
    TABLE_TRANSACTIONS: 'transactions',
};
