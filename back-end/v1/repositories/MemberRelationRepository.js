const DBConnector = require('../../API/DBConnector');
const LogManager = require('../../utils/LogManager');
const MemberRelationQuery = require('../queries/MemberRelationQuery');
let logger = LogManager.getLogger('MemberRelationRepository');

class MemberRelationRepository {
    #connection = null;
    constructor(connection) {
        this.#connection = connection;
    }

    async mappingRelation(memberId, refId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberRelationQuery.INSERT_MEMBER_RELATION, [memberId, refId]);
            logger.debug('RESULT : ', result);
            return result;
        } catch (error) {
            logger.error('MemberRelationRepository mappingRelation() Exception :', error);
            throw error;
        }
    }

    async countChildMember(memberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberRelationQuery.COUNT_REF_MEMBER, [memberId]);
            logger.debug('RESULT : ', result);
            return result.data[0].TOTAL_CHILD;
        } catch (error) {
            logger.error('MemberRelationRepository countChildMember() Exception :', error);
            throw error;
        }
    }

    async findRefId(memberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberRelationQuery.FIND_REF_ID, [memberId]);
            logger.debug('RESULT : ', result);
            if (result.data.length > 0) {
                return result.data[0].ref_id;
            }

            return null;
        } catch (error) {
            logger.error('MemberRelationRepository countChildMember() Exception :', error);
            throw error;
        }
    }
}

module.exports = MemberRelationRepository;
