const DBConnector = require('../../API/DBConnector');
let LogManager = require('../../utils/LogManager');
const MemberQuery = require('../queries/MemberQuery');
let logger = LogManager.getLogger('MemberRepository');

class MemberRepository {
    #connection = null;

    constructor(connection) {
        this.#connection = connection;
    }

    async createMember(params) {
        try {
            logger.debug('PARAMS : ', params);
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.INSERT_MEMBER, params);
            return result;
        } catch (error) {
            logger.error('MemberRepository createMember() Exception :', error);
            throw error;
        }
    }

    async editMember(params) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.EDIT_MEMBER, params);
            return result;
        } catch (error) {
            logger.error('MemberRepository editMember() Exception :', error);
            throw error;
        }
    }

    async login(username, password) {
        try {
            logger.info(password);
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.LOGIN, [username, username, password]);
            logger.info(result);
            return result;
        } catch (error) {
            logger.error('MemberRepository login() Exception :', error);
            throw error;
        }
    }

    async isUserExist(username) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.CHECK_MEMBER_EXIST, [username, username]);
            logger.debug(result);
            let isExist = result.data[0].NUM > 0;
            result.data[0].NUM = undefined;
            return { isExist: isExist, data: result.data[0] };
        } catch (error) {
            logger.error('MemberRepository isUserExist() Exception :', error);
            throw error;
        }
    }

    async checkCardValid(memberId, cardId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.CHECK_CARD_ID_VALID, [memberId, cardId]);
            let isExist = result.data[0].NUM > 0;
            return isExist;
        } catch (error) {
            logger.error('MemberRepository checkCardValid() Exception :', error);
            throw error;
        }
    }

    async findAllMemberUnderTeam(memberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.FIND_ALL_MEMBER_UNDER_TEAM, [memberId]);
            return result.data;
        } catch (error) {
            logger.error('MemberRepository findAllMemberUnderTeam() Exception:', error);
            throw error;
        }
    }

    async findMemberInfo(memberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.FIND_MEMBER_INFO, [memberId]);
            return result.data.length > 0 ? result.data[0] : {};
        } catch (error) {
            logger.error('MemberRepository findMemberInfo() Exception:', error);
            throw error;
        }
    }

    async findLastMemberId() {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.GET_LAST_MEMBER_ID, []);
            return result.data[0].ID;
        } catch (error) {
            logger.error('MemberRepository findAllMemberUnderTeam() Exception:', error);
            throw error;
        }
    }

    async searchMember(params) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.SEARCH_MEMBER, params);
            return result.data;
        } catch (error) {
            logger.error('MemberRepository searchMember() Exception:', error);
            throw error;
        }
    }

    async changePassword(memberId, newPassword) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, MemberQuery.CHANGE_PASSWORD, [newPassword, memberId]);
            return result.data;
        } catch (error) {
            logger.error('MemberRepository changePassword() Exception:', error);
            throw error;
        }
    }
}

module.exports = MemberRepository;
