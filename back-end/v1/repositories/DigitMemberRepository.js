const DBConnector = require('../../API/DBConnector');
let LogManager = require('../../utils/LogManager');
const DigitMemberQuery = require('../queries/DigitMemberQuery');
let logger = LogManager.getLogger('MemberRepository');

class DigitMemberRepository {
    #connection = null;

    constructor(connection) {
        this.#connection = connection;
    }

    async updatePrefixNum(val, digitMemberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, DigitMemberQuery.UPDATE_DIGIT_MEMBER_RUN_PREFIX_NUMBER, [val, digitMemberId]);
            return result;
        } catch (error) {
            logger.error('DigitMemberRepository updatePrefixNum() Exception :', error);
            throw error;
        }
    }

    async updateSequence(val, digitMemberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, DigitMemberQuery.UPDATE_DIGIT_MEMBER_RUN_SEQUENCE_NUMBER, [val, digitMemberId]);
            return result;
        } catch (error) {
            logger.error('DigitMemberRepository updateSequence() Exception :', error);
            throw error;
        }
    }

    async updatePrefix(prefix, digitMemberId) {
        try {
            let result = await DBConnector.executeQuery(this.#connection, DigitMemberQuery.UPDATE_DIGIT_MEMBER_RUN_PREFIX, [prefix, digitMemberId]);
            return result;
        } catch (error) {
            logger.error('DigitMemberRepository updateSequence() Exception :', error);
            throw error;
        }
    }

    async getDigitMember() {
        try {
            let result = await DBConnector.executeQuery(this.#connection, DigitMemberQuery.GET_DIGIT_MEMBER, []);
            return result;
        } catch (error) {
            logger.error('DigitMemberRepository updateSequence() Exception :', error);
            throw error;
        }
    }
}

module.exports = DigitMemberRepository;
