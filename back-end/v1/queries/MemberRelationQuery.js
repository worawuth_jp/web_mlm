const dataTable = require('../constants/dataTable');

module.exports = {
    INSERT_MEMBER_RELATION: `INSERT INTO ${dataTable.TABLE_MEMBER_RELATION} (member_id,ref_id) VALUES (?,?)`,
    COUNT_REF_MEMBER: `SELECT COUNT(*) AS TOTAL_CHILD FROM ${dataTable.TABLE_MEMBER_RELATION} WHERE ref_id=?`,
    FIND_REF_ID: `SELECT ref_id FROM ${dataTable.TABLE_MEMBER_RELATION} WHERE member_id=?`,
};
