const dataTable = require('../constants/dataTable');

module.exports = {
    INSERT_MEMBER: `INSERT INTO ${dataTable.TABLE_MEMBERS} (member_code,card_id, member_phone, member_password, first_name, last_name, birth_date, bank, bank_account, address)
    VALUES (?,?,?,?,?,?,?,?,?,?)`,
    EDIT_MEMBER: `UPDATE ${dataTable.TABLE_MEMBERS} SET card_id=?, member_phone=?, first_name=?, last_name=?, birth_date=?, bank=?, bank_account=?, address=? WHERE member_id=?`,
    CHECK_MEMBER_EXIST: `SELECT COUNT(*) as NUM, members.member_id, members.member_code, members.card_id, members.member_phone, member_password, first_name, last_name FROM ${dataTable.TABLE_MEMBERS} members WHERE members.member_phone = ? OR members.card_id = ?`,
    CHECK_CARD_ID_VALID: `SELECT COUNT(*) as NUM FROM ${dataTable.TABLE_MEMBERS} members WHERE members.member_id = ? AND members.card_id = ?`,
    LOGIN: `SELECT COUNT(*) as NUM FROM ${dataTable.TABLE_MEMBERS} members WHERE (members.member_phone = ? OR members.card_id = ?) AND members.member_password=?`,

    FIND_ALL_MEMBER_UNDER_TEAM: `
        SELECT 
        members.member_id,
        members.member_code,
        members.card_id,
        members.member_phone,
        members.first_name,
        members.last_name,
        members.birth_date,
        banks.bank_id,
        banks.bank_name,
        members.bank_account,
        mr.ref_id
    FROM
        ${dataTable.TABLE_MEMBERS} members
            INNER JOIN
        ${dataTable.TABLE_MEMBER_RELATION} mr ON mr.member_id = members.member_id
            INNER JOIN
        ${dataTable.TABLE_BANKS} banks ON banks.bank_id = members.bank
    WHERE
        mr.ref_id = ? AND members.enable = 'Y'
            AND mr.enable = 'Y'
    `,
    FIND_MEMBER_INFO: `
        SELECT 
        members.member_id,
        members.member_code,
        members.card_id,
        members.member_phone,
        members.first_name,
        members.last_name,
        members.birth_date,
        banks.bank_id,
        banks.bank_name,
        members.bank_account
    FROM
        ${dataTable.TABLE_MEMBERS} members
            INNER JOIN
        ${dataTable.TABLE_BANKS} banks ON banks.bank_id = members.bank
    WHERE members.enable = 'Y' AND members.member_id = ?
    `,
    GET_LAST_MEMBER_ID: `
        SELECT member_id AS ID FROM ${dataTable.TABLE_MEMBERS} ORDER BY member_id desc LIMIT 1
    `,

    SEARCH_MEMBER: `
        SELECT 
            members.member_id,
            members.member_code,
            members.card_id,
            members.member_phone,
            members.first_name,
            members.last_name,
            members.birth_date,
            banks.bank_id,
            banks.bank_name,
            members.bank_account
        FROM
            ${dataTable.TABLE_MEMBERS} members
                INNER JOIN
            ${dataTable.TABLE_BANKS} banks ON banks.bank_id = members.bank
        WHERE members.enable = 'Y' AND (members.member_code LIKE ? OR members.first_name LIKE ? OR members.last_name LIKE ? OR members.card_id LIKE ? OR members.member_phone LIKE ?)
     `,

    CHANGE_PASSWORD: `
        UPDATE ${dataTable.TABLE_MEMBERS} SET member_password=? WHERE member_id=?
    `,
};
