const dataTable = require('../constants/dataTable');

module.exports = {
    UPDATE_DIGIT_MEMBER_RUN_PREFIX_NUMBER: `
        UPDATE ${dataTable.TABLE_DIGIT_MEMBER} SET prefix_num = ? WHERE digit_member_id = ?
    `,
    UPDATE_DIGIT_MEMBER_RUN_SEQUENCE_NUMBER: `UPDATE ${dataTable.TABLE_DIGIT_MEMBER} SET sequence =? WHERE digit_member_id = ?`,
    UPDATE_DIGIT_MEMBER_RUN_PREFIX: `UPDATE ${dataTable.TABLE_DIGIT_MEMBER} SET prefix=? WHERE digit_member_id = ?`,
    GET_DIGIT_MEMBER: `SELECT * FROM ${dataTable.TABLE_DIGIT_MEMBER} ORDER BY digit_member_id DESC LIMIT 1`,
};
