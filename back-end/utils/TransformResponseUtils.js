const { ceil } = require('lodash');
const http = require('../constants/http');
const ResponseModel = require('../models/response/ResonseModel');
const ResponseError = require('../models/response/ResponseError');
const LogManager = require('./LogManager');

const logger = LogManager.getLogger('TransformResponseUtil');

class TransformResponseUtil {
    toResponseError(error) {
        const responseError = new ResponseError();
        responseError.statusCode = error.code;
        responseError.statusMessage = error.msg;
        responseError.stack = error.stack && !error.stack.http ? error.stack : undefined;
        return responseError;
    }

    toResponseSuccess() {
        const responseSuccess = new ResponseModel();
        responseSuccess.statusCode = http.HTTP_SUCCESS_CODE;
        responseSuccess.statusMessage = http.HTTP_SUCCESS_MSG;
        responseSuccess.result = void 0;
        return responseSuccess;
    }

    toResponseSuccessWithData(data) {
        const responseSuccess = new ResponseModel();
        responseSuccess.statusCode = http.HTTP_SUCCESS_CODE;
        responseSuccess.statusMessage = http.HTTP_SUCCESS_MSG;
        responseSuccess.result = data;
        return responseSuccess;
    }

    toResponseSuccessWithPaginate(data, totalRecord, pageNo, pageSize) {
        const responseSuccess = new ResponseModel();
        responseSuccess.statusCode = http.HTTP_SUCCESS_CODE;
        responseSuccess.statusMessage = http.HTTP_SUCCESS_MSG;
        responseSuccess.result = {
            page: pageNo,
            pageSize: pageSize,
            totalRecord: totalRecord,
            totalPage: pageSize > 0 ? Math.ceil(totalRecord / pageSize) : 1,
            data: data,
        };
        return responseSuccess;
    }
}

module.exports = new TransformResponseUtil();
