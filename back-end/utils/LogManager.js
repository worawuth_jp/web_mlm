require('dotenv').config({ path: `../.env.${process.env.NODE_ENV}` });
const log4js = require('log4js');
var httpContext = require('express-http-context');
class LogManager {
    constructor() {
        log4js.addLayout('pattern', function (config) {
            return function (logEvent) {
                let logMessage = '';
                for (let i = 0, len = logEvent['data'].length; i < len; i++) {
                    logMessage += JSON.stringify(logEvent['data'][i]) + ' ';
                }
                let requestAPIVersionHttpContext = httpContext.get('x-api-version');
                let requestAPIVersion = requestAPIVersionHttpContext !== void 0 && requestAPIVersionHttpContext !== null ? requestAPIVersionHttpContext : 'default';
                let startTime = logEvent.startTime;
                let startTimeString =
                    startTime.getDate().toString().padStart(2, '0') +
                    '/' +
                    (startTime.getMonth() + 1).toString().padStart(2, '0') +
                    '/' +
                    startTime.getFullYear() +
                    'T' +
                    startTime.getHours().toString().padStart(2, '0') +
                    ':' +
                    startTime.getMinutes().toString().padStart(2, '0') +
                    ':' +
                    startTime.getSeconds().toString().padStart(2, '0');
                let pattern = '[' + startTimeString + ']' + '[' + logEvent.categoryName + '] [' + logEvent.level.levelStr + '] [' + requestAPIVersion + '] ' + logMessage;
                return pattern;
            };
        });
        log4js.configure('./Configurations/log4js-conf.json');
    }

    getLogger(classname) {
        let logger = log4js.getLogger(classname);
        logger.level = process.env.LOG_LEVEL;
        return logger;
    }
}

module.exports = new LogManager();
