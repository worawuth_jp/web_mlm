let express = require('express');
const path = require('path');
let routesVersioning = require('express-routes-versioning')();
const multiparty = require('connect-multiparty');
const router = express.Router();
const CommonEventRouter = require('./eventRouters/CommonEventRouter');

const dayjs = require('dayjs');
const http = require('../constants/http');
const MemberEventRouter = require('./eventRouters/MemberEventRouter');

require('dotenv').config({ path: path.join(__dirname, `../.env.${process.env.NODE_ENV}`) });

router.post(
    '/members',
    routesVersioning(
        {
            '1.0.0': MemberEventRouter.createMember,
        },
        CommonEventRouter.versionNotMatch
    )
);

router.put(
    '/members',
    routesVersioning(
        {
            '1.0.0': MemberEventRouter.editMember,
        },
        CommonEventRouter.versionNotMatch
    )
);

router.post(
    '/login',
    routesVersioning(
        {
            '1.0.0': MemberEventRouter.login,
        },
        CommonEventRouter.versionNotMatch
    )
);

router.post(
    '/change-password',
    routesVersioning(
        {
            '1.0.0': MemberEventRouter.changePassword,
        },
        CommonEventRouter.versionNotMatch
    )
);

router.get(
    '/generate-member-code',
    routesVersioning(
        {
            '1.0.0': MemberEventRouter.generate,
        },
        CommonEventRouter.versionNotMatch
    )
);

router.get(
    '/member-under-team',
    routesVersioning(
        {
            '1.0.0': MemberEventRouter.getMemberUnderTeam,
        },
        CommonEventRouter.versionNotMatch
    )
);
module.exports = router;
