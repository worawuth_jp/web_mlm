let express = require('express');
const path = require('path');
let routesVersioning = require('express-routes-versioning')();
const multiparty = require('connect-multiparty');
const router = express.Router();
const CommonEventRouter = require('./eventRouters/CommonEventRouter');
const IndexEventRouter = require('./eventRouters/IndexEventRouter');

const dayjs = require('dayjs');
const http = require('../constants/http');

require('dotenv').config({ path: path.join(__dirname, `../.env.${process.env.NODE_ENV}`) });

router.get(
    '/hello',
    routesVersioning(
        {
            '1.0.0': IndexEventRouter.hello,
        },
        CommonEventRouter.versionNotMatch
    )
);
module.exports = router;
