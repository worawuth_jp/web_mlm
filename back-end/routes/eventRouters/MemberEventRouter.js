const dayjs = require('dayjs');
const http = require('../../constants/http');
const ErrorModel = require('../../models/response/ErrorModel');
const LogManager = require('../../utils/LogManager');
const TransformResponseUtils = require('../../utils/TransformResponseUtils');
const DigitMemberController = require('../../v1/controller/DigitMemberController');
const MemberController = require('../../v1/controller/MemberController');
const logger = LogManager.getLogger('MemberEventRouter');

class MemberEventRouter {
    async createMember(req, res, next) {
        try {
            let responseModel = await new MemberController().createMember(req.body);
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('MemberEventRouter createMember() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(TransformResponseUtils.toResponseError(error));
            } else {
                res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
            }
        }
    }

    async editMember(req, res, next) {
        try {
            let responseModel = await new MemberController().editMember(req.body);
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('MemberEventRouter editMember() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(TransformResponseUtils.toResponseError(error));
            } else {
                res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
            }
        }
    }

    async login(req, res, next) {
        try {
            let responseModel = await new MemberController().login(req.body);
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('MemberEventRouter login() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(TransformResponseUtils.toResponseError(error));
            } else {
                res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
            }
        }
    }

    async generate(req, res, next) {
        try {
            let responseModel = await new DigitMemberController().generateDigitMember(req.params);
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('MemberEventRouter generate() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(TransformResponseUtils.toResponseError(error));
            } else {
                res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
            }
        }
    }

    async getMemberUnderTeam(req, res, next) {
        try {
            let responseModel = await new MemberController().getMemberUnderTeam(req.query);
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('MemberEventRouter getMemberUnderTeam() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(TransformResponseUtils.toResponseError(error));
            } else {
                res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
            }
        }
    }

    async changePassword(req, res, next) {
        try {
            let responseModel = await new MemberController().changePassword(req.body);
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('MemberEventRouter changePassword() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(TransformResponseUtils.toResponseError(error));
            } else {
                res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
            }
        }
    }
}

module.exports = new MemberEventRouter();
