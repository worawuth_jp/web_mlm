const dayjs = require('dayjs');
const http = require('../../constants/http');
const ErrorModel = require('../../models/response/ErrorModel');
const ResponseModel = require('../../models/response/ResonseModel');
const LogManager = require('../../utils/LogManager');
const logger = LogManager.getLogger('IndexEventRouter');

class IndexEventRouter {
    async hello(req, res, next) {
        try {
            const responseModel = new ResponseModel();
            responseModel.statusCode = http.HTTP_SUCCESS_CODE;
            responseModel.statusMessage = http.HTTP_SUCCESS_MSG;
            res.status(http.HTTP_SUCCESS_CODE).send(responseModel);
        } catch (error) {
            logger.error('IndexEventRouter hello() Exception: ', error);
            if (error.http) {
                res.status(error.http).send(error);
            }
            res.status(http.HTTP_INTERNAL_SERVER_CODE).send(new ErrorModel(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG));
        }
    }
}

module.exports = new IndexEventRouter();
