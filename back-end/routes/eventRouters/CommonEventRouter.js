const http = require('../../constants/http');
const CommonResponse = require('../../models/response/CommonResponse');

class CommonEventRouter {
    async versionNotMatch(req, res, next) {
        try {
            const commonResponse = new CommonResponse();
            commonResponse.statusCode = http.HTTP_VERSION_ERROR_CODE;
            commonResponse.statusMessage = http.HTTP_VERSION_ERROR_MSG;
            res.status(http.HTTP_VERSION_ERROR_CODE).send(commonResponse);
        } catch (error) {}
    }
}

module.exports = new CommonEventRouter();
