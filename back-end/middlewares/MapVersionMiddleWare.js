let httpContext = require('express-http-context')

module.exports = {
    mapVersioning: (req, res, next) => {
        const xApiVersion = req.headers['x-api-version']
        if(xApiVersion !== void 0 && xApiVersion !== 0 && xApiVersion !== null && xApiVersion !== ""){
            req.version = xApiVersion
        }
        httpContext.set('x-api-version', xApiVersion)
        httpContext.set('accept-version', xApiVersion)
        next()
    }
}