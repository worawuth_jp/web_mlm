const http = require('../constants/http');
const ApplicationError = require('../models/errors/ApplicationError');
const LogManager = require('../utils/LogManager');
const transformResponseUtils = require('../utils/TransformResponseUtils');
const httpContext = require('express-http-context');
const jwt = require('jsonwebtoken');
const { toResponseError } = require('../utils/TransformResponseUtils');
// const UserService = require('../v1/services/UserService');

const logger = LogManager.getLogger('Authorization');
const verifyToken = async(req, res, next) => {
    try {
        let beaerToken = req.headers['authorization'];
        let userService = new UserService();
        if (beaerToken !== void 0) {
            logger.info('Token : ', beaerToken);
            let beaderList = beaerToken.split(' ');
            let token = beaderList[1];
            let secret = req.headers['secret-key'];
            req.token = token;
            httpContext.set('TOKEN', token);
            httpContext.set('secret-key', secret);
            // let check = await userService.checkSecretKey(token,secret);
            if(!check.valid){
                throw new ApplicationError(http.HTTP_FORBIDDEN_ERROR_CODE, http.HTTP_FORBIDDEN_ERROR_CODE, http.HTTP_FORBIDDEN_ERROR_MSG,{msg:'secretkey invalid.',code:'INVALID_SECRET_KEY'});
            }
            jwt.verify(token, secret, (err, authData) => {
                if (err) {
                    throw new ApplicationError(http.HTTP_FORBIDDEN_ERROR_CODE, http.HTTP_FORBIDDEN_ERROR_CODE, http.HTTP_FORBIDDEN_ERROR_MSG, err);
                }
                logger.info(authData);
                httpContext.set('auth-data', authData);
                next();
            });
            //next();
        } else {
            res.status(http.HTTP_UNAUTHORIZE_ERROR_CODE).send(
                transformResponseUtils.toResponseError(new ApplicationError(http.HTTP_UNAUTHORIZE_ERROR_CODE, http.HTTP_UNAUTHORIZE_ERROR_CODE, http.HTTP_UNAUTHORIZE_ERROR_MSG))
            );
        }
    } catch (error) {
        logger.error('Authorization exception: ', error);
        if (!error.code) {
            res.status(http.HTTP_INTERNAL_SERVER_CODE).send(toResponseError(new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG)));
        }
        //throw new ApplicationError(http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_CODE, http.HTTP_INTERNAL_SERVER_MSG, error);
        res.status(error.code).send(transformResponseUtils.toResponseError(error));
    }
};

module.exports = {
    verifyToken: verifyToken,
};
