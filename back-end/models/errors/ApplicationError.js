class ApplicationError extends Error {
    constructor(http, code, msg, stack) {
        super(http, code, msg, stack);
        this.http = http;
        this.code = code;
        this.msg = msg;
        this.stack = stack;
    }
}

module.exports = ApplicationError;
