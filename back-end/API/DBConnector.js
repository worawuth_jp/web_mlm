var mysql = require('mysql2/promise');
let LogManager = require('../utils/LogManager');
let logger = LogManager.getLogger('DBConnector');

class DBConnector {
    #con = null;

    async getConnector() {
        try {
            this.#con = await mysql.createConnection({
                host: process.env.DB_HOST || 'localhost',
                user: process.env.DB_USERNAME || 'root',
                password: process.env.DB_PASSWORD || '',
                port: process.env.DB_PORT || '8000',
                database: process.env.DB_NAME || 'mlm_db',
            });
            // if (this.#con === null) {
            //     await this.#con.connect(function (err) {
            //         if (err) throw err;
            //         logger.debug('Connected!');
            //     });
            // }
            logger.debug('[CONNECTOR] START');
            return this.#con;
        } catch (error) {
            logger.error('[CONNECTOR] ERROR');
            throw error;
        }
    }

    async executeQuery(connection, sql, params = []) {
        try {
            let [rows, fields] = await connection.execute(sql, params);
            return { code: 'SUCCESS', data: rows };
        } catch (error) {
            logger.error('executeQuery Error : ', error);
            throw error;
        }
    }

    async commit(con) {
        try {
            await con.commit();
            logger.debug('[CONNECTION] COMMIT SUCCESS');
        } catch (error) {
            logger.error('[CONNECTION] COMMIT ERROR');
            throw error;
        }
    }

    async releaseConnector(con) {
        try {
            //await con.destroy();
            await con.end();
            logger.debug('[CONNECTOR] END');
        } catch (error) {
            logger.error('[CONNECTOR] ERROR');
            throw error;
        }
    }
}

module.exports = new DBConnector();
