-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jun 16, 2022 at 09:04 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mlm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `bank_id` int(11) NOT NULL,
  `bank_name` varchar(250) DEFAULT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`bank_id`, `bank_name`, `enable`, `created_at`, `updated_at`) VALUES
(1, 'ธนาคารกรุงเทพ จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(2, 'ธนาคารกรุงไทย จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(3, 'ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(4, 'ธนาคารกสิกรไทย จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(5, 'ธนาคารเกียรตินาคินภัทร จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(6, 'ธนาคารซีไอเอ็มบี ไทย จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(7, 'ธนาคารทหารไทยธนชาต จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(8, 'ธนาคารทิสโก้ จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(9, 'ธนาคารไทยพาณิชย์ จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(10, 'ธนาคารยูโอบี จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(11, 'นาคารแลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(12, 'ธนาคารสแตนดาร์ดชาร์เตอร์ด (ไทย) จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(13, 'ธนาคารไอซีบีซี (ไทย) จำกัด (มหาชน)', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(14, 'ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(15, 'ธนาคารออมสิน', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(16, 'ธนาคารอาคารสงเคราะห์', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54'),
(17, 'ธนาคารซิตี้แบงก์ เอ็น.เอ. สาขากรุงเทพฯ', 'Y', '2022-06-14 06:19:54', '2022-06-14 06:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `digit_member`
--

CREATE TABLE `digit_member` (
  `digit_member_id` int(11) NOT NULL,
  `prefix` varchar(5) NOT NULL,
  `prefix_num` int(11) NOT NULL DEFAULT 1,
  `prefix_digits` int(11) DEFAULT 3,
  `sequence` int(11) NOT NULL DEFAULT 1,
  `sequence_digits` int(11) NOT NULL DEFAULT 5,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `digit_member`
--

INSERT INTO `digit_member` (`digit_member_id`, `prefix`, `prefix_num`, `prefix_digits`, `sequence`, `sequence_digits`, `enable`, `created_at`, `updated_at`) VALUES
(1, '0', 1, 2, 1, 5, 'Y', '2022-06-16 17:27:12', '2022-06-16 17:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `extra_commission`
--

CREATE TABLE `extra_commission` (
  `extra_commission_id` bigint(50) NOT NULL,
  `member_id` bigint(50) NOT NULL,
  `commission` decimal(22,2) NOT NULL DEFAULT 1.00,
  `commission_unit` decimal(22,2) NOT NULL DEFAULT 1.00,
  `month` int(11) NOT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `member_id` bigint(50) NOT NULL,
  `member_code` varchar(50) DEFAULT NULL,
  `card_id` varchar(20) NOT NULL,
  `member_phone` varchar(15) NOT NULL,
  `member_password` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `bank` int(11) NOT NULL,
  `bank_account` varchar(50) NOT NULL,
  `address` longtext DEFAULT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`member_id`, `member_code`, `card_id`, `member_phone`, `member_password`, `first_name`, `last_name`, `birth_date`, `bank`, `bank_account`, `address`, `enable`, `created_at`, `updated_at`) VALUES
(1, '00100001', 'admin', '0000000000', '$2b$10$ByVLB0AbKsRR6PnSTwq2yu6jO6jdRRPdFWZh4TFy9cnQq6PjgpQg.', 'admin', 'test', '0000-00-00', 1, '0000000000', '123/12 Moo.3', 'Y', '2022-06-13 19:46:13', '2022-06-13 19:46:13');

-- --------------------------------------------------------

--
-- Table structure for table `member_relation`
--

CREATE TABLE `member_relation` (
  `member_relation_id` bigint(50) NOT NULL,
  `member_id` bigint(50) NOT NULL,
  `ref_id` bigint(50) NOT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` bigint(50) NOT NULL,
  `order_no` varchar(50) NOT NULL,
  `member_id` bigint(50) NOT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_detail_id` bigint(50) NOT NULL,
  `order_id` bigint(50) NOT NULL,
  `product_id` bigint(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `price` decimal(22,2) NOT NULL,
  `profit` decimal(22,2) NOT NULL,
  `gp` decimal(22,2) NOT NULL,
  `commission` decimal(22,2) NOT NULL DEFAULT 1.00,
  `commission_unit` decimal(22,2) NOT NULL DEFAULT 1.00,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` bigint(50) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `sell_price` decimal(22,4) NOT NULL,
  `ref_price` decimal(22,4) NOT NULL,
  `commission` decimal(22,2) NOT NULL DEFAULT 1.00,
  `commission_unit` decimal(22,2) NOT NULL DEFAULT 1.00 COMMENT 'Baht=1 , %(/100) = 0.0-0.99',
  `gp` decimal(22,2) NOT NULL,
  `profit` decimal(22,4) NOT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` bigint(50) NOT NULL,
  `transaction_no` varchar(50) NOT NULL,
  `transaction_type` char(1) NOT NULL DEFAULT 'D' COMMENT 'D=Deposit/W=Withdraw',
  `wallet_id` bigint(50) NOT NULL,
  `amount` decimal(22,4) NOT NULL,
  `balance` decimal(22,4) NOT NULL,
  `from` varchar(250) DEFAULT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `wallet_id` bigint(50) NOT NULL,
  `member_id` bigint(50) NOT NULL,
  `wallet_name` varchar(150) DEFAULT NULL,
  `wallet_balance` decimal(25,4) NOT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `digit_member`
--
ALTER TABLE `digit_member`
  ADD PRIMARY KEY (`digit_member_id`);

--
-- Indexes for table `extra_commission`
--
ALTER TABLE `extra_commission`
  ADD PRIMARY KEY (`extra_commission_id`),
  ADD KEY `commission` (`commission`),
  ADD KEY `enable` (`enable`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `card_id` (`card_id`),
  ADD KEY `bank_account` (`bank_account`),
  ADD KEY `member_phone` (`member_phone`),
  ADD KEY `member_password` (`member_password`),
  ADD KEY `bank` (`bank`);

--
-- Indexes for table `member_relation`
--
ALTER TABLE `member_relation`
  ADD PRIMARY KEY (`member_relation_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `ref_id` (`ref_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_no` (`order_no`),
  ADD KEY `enable` (`enable`),
  ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_detail_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_code` (`product_code`),
  ADD KEY `product_name` (`product_name`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `wallet_id` (`wallet_id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`wallet_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `wallet_name` (`wallet_name`),
  ADD KEY `wallet_balance` (`wallet_balance`),
  ADD KEY `enable` (`enable`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `digit_member`
--
ALTER TABLE `digit_member`
  MODIFY `digit_member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `extra_commission`
--
ALTER TABLE `extra_commission`
  MODIFY `extra_commission_id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `member_id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `member_relation`
--
ALTER TABLE `member_relation`
  MODIFY `member_relation_id` bigint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `order_detail_id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `wallet_id` bigint(50) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `extra_commission`
--
ALTER TABLE `extra_commission`
  ADD CONSTRAINT `extra_commission_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`bank`) REFERENCES `banks` (`bank_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `member_relation`
--
ALTER TABLE `member_relation`
  ADD CONSTRAINT `member_relation_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `member_relation_ibfk_2` FOREIGN KEY (`ref_id`) REFERENCES `members` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`wallet_id`) REFERENCES `wallets` (`wallet_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wallets`
--
ALTER TABLE `wallets`
  ADD CONSTRAINT `wallets_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
